package com.ftn.KnjizaraOWP.service;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.ftn.KnjizaraOWP.model.Knjiga;

@Service
public interface  ShoppingCartService {
    void addProduct(Knjiga knjiga, int kolicina);
    void removeProduct(Knjiga knjiga);
    void clearProducts();
    Map<Knjiga, Integer> productsInCart();
    BigDecimal  totalPrice();
    void cartCheckout();
    Integer numberEl();

}
