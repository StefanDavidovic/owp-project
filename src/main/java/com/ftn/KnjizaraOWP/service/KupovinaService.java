package com.ftn.KnjizaraOWP.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ftn.KnjizaraOWP.model.Kupovina;


public interface KupovinaService {
	Kupovina findOne(int id);
	
	Kupovina findOne(LocalDate datumKupovine);

	
	List<Kupovina> findAll();
	
	List<Kupovina> findAll2();
	
	Kupovina deleteAll();
	
	Kupovina save(Kupovina kupovina);
	
	List<Kupovina> save(List<Kupovina> kupovine);
	
	Kupovina update(Kupovina kupovina);
	
	List<Kupovina> update(List<Kupovina> kupovine);
	
	Kupovina delete(Integer id);
	
	List<Kupovina> find(int id, int knjigaId, String musterijaOznaka);
	
	List<Kupovina> find(int knjigaId);
	
	List<Kupovina> findd(Long musterijaOznaka);
	
	List<Kupovina> find2(LocalDate datumKupovineOd, LocalDate datumKupovineDo);

}
