package com.ftn.KnjizaraOWP.service;

import java.util.List;

import com.ftn.KnjizaraOWP.model.LoyaltyKartica;

public interface LoyaltyKarticaService {

	LoyaltyKartica find(int id);
	LoyaltyKartica findOne(int vlasnikOznaka);
	List<LoyaltyKartica> findAll();
	List<LoyaltyKartica> findAll2();
	LoyaltyKartica save(LoyaltyKartica loyaltyKartica);
	List<LoyaltyKartica> save(List<LoyaltyKartica> loyaltyKartice);
	LoyaltyKartica update(LoyaltyKartica loyaltyKartica);
	List<LoyaltyKartica> update(List<LoyaltyKartica> loyaltyKartice);
	int delete(int id);
	void delete(List<String> id);
	List<LoyaltyKartica> find(int id, String popust, int brPoena,
			String vlasnikOznaka, String status);
	
	
	List<LoyaltyKartica> findById(int id); 

}
