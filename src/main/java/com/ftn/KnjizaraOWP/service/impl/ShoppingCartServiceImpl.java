package com.ftn.KnjizaraOWP.service.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.ftn.KnjizaraOWP.model.Knjiga;
import com.ftn.KnjizaraOWP.service.ShoppingCartService;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class ShoppingCartServiceImpl implements ShoppingCartService{
	
    private Map<Knjiga, Integer> cart = new LinkedHashMap<>();

	@Override
	public void addProduct(Knjiga knjiga, int kolicina) {
        if (cart.containsKey(knjiga)){
            cart.replace(knjiga, cart.get(knjiga) +  kolicina);
        }else{
            cart.put(knjiga, kolicina);
        }		
	}

    @Override
    public void removeProduct(Knjiga knjiga) {
    	System.out.println(cart);
    	System.out.println(knjiga);
		System.out.println(cart.get(knjiga));
		cart.remove(knjiga);
        if (cart.containsKey(knjiga)) {
    		System.out.println("ovo je u service"  + knjiga.getId());
            if (cart.get(knjiga) > 1)
                cart.replace(knjiga, cart.get(knjiga) - 1);
            else if (cart.get(knjiga) == 1) {
                cart.remove(knjiga);
            }
        }
    }

	@Override
	public void clearProducts() {
        cart.clear();
		
	}

	@Override
	public Map<Knjiga, Integer> productsInCart() {
        return Collections.unmodifiableMap(cart);
	}


	@Override
	public void cartCheckout() {
        cart.clear();		
	}

	@Override
	public BigDecimal totalPrice() {
        return cart.entrySet().stream()
                .map(k -> k.getKey().getCena().multiply(BigDecimal.valueOf(k.getValue()))).sorted()
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
	}

	@Override
	public Integer numberEl() {
		return cart.size();
	}

}
