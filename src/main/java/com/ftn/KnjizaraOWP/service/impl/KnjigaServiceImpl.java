package com.ftn.KnjizaraOWP.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.KnjizaraOWP.dao.KnjigaDao;
import com.ftn.KnjizaraOWP.model.EPismo;
import com.ftn.KnjizaraOWP.model.EPovez;
import com.ftn.KnjizaraOWP.model.Knjiga;
import com.ftn.KnjizaraOWP.service.KnjigaService;

@Service
public class KnjigaServiceImpl implements KnjigaService{
	
	@Autowired
	private KnjigaDao knjigaDao;

	@Override
	public Knjiga findOne(long id) {
		return knjigaDao.findOne(id);
	}

	@Override
	public List<Knjiga> findAll() {

		return knjigaDao.findAll();
	}

	@Override
	public List<Knjiga> find(Long id, String naziv, String isbn, String izdavackaKuca, String autor,String opis, String jezik, Double cenaOd, Double cenaDo, Double prosecnaOcena, Integer brStrana,Integer kolicina, String povez, String pismo, String slika, Date datumOd, Date datumDo, Integer popust) {
		return knjigaDao.find(id, naziv, isbn, izdavackaKuca, autor, jezik,opis, cenaOd, cenaDo, prosecnaOcena, brStrana,kolicina, povez, pismo, slika, datumOd, datumDo, popust);
	}

	@Override
	public int save(Knjiga knjiga) {
		return knjigaDao.save(knjiga);
	}

	@Override
	public int update(Knjiga knjiga) {
		return knjigaDao.update(knjiga);
	}

	@Override
	public int delete(long id) {
		return knjigaDao.delete(id);
	}

	@Override
	public List<Knjiga> sortirajPoNazivu() {
		return knjigaDao.sortirajPoNazivu();
	}



}
