package com.ftn.KnjizaraOWP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.KnjizaraOWP.dao.LoyaltyKarticaDAO;
import com.ftn.KnjizaraOWP.model.LoyaltyKartica;
import com.ftn.KnjizaraOWP.service.LoyaltyKarticaService;

@Service
public class LoyaltyKarticaServiceImpl implements LoyaltyKarticaService {

	@Autowired
	private LoyaltyKarticaDAO loyaltyKarticaDAO;
	
	
	@Override
	public LoyaltyKartica find(int id) {
		return loyaltyKarticaDAO.findOne(id);
	}


	@Override
	public List<LoyaltyKartica> findAll() {
		return loyaltyKarticaDAO.findAll();
	}

	@Override
	public LoyaltyKartica save(LoyaltyKartica loyaltyKartica) {
		loyaltyKarticaDAO.save(loyaltyKartica);
		return loyaltyKartica;
	}

	@Override
	public List<LoyaltyKartica> save(List<LoyaltyKartica> komentari) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LoyaltyKartica update(LoyaltyKartica loyaltyKartica) {
		loyaltyKarticaDAO.update(loyaltyKartica);
		return loyaltyKartica;
	}

	@Override
	public List<LoyaltyKartica> update(List<LoyaltyKartica> komentari) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(int id) {
		return loyaltyKarticaDAO.delete(id);
	}

	@Override
	public void delete(List<String> id) {
		
		
		
	}

	@Override
	public List<LoyaltyKartica> find(int id, String popust, int brPoena, String vlasnikOznaka, String status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LoyaltyKartica> findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public LoyaltyKartica findOne(int vlasnikOznaka) {
		return loyaltyKarticaDAO.findOne(vlasnikOznaka);
	}


	@Override
	public List<LoyaltyKartica> findAll2() {
		return loyaltyKarticaDAO.findAll2();
	}


}
