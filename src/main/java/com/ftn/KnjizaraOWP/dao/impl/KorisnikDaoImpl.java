package com.ftn.KnjizaraOWP.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.KnjizaraOWP.dao.KorisnikDao;
import com.ftn.KnjizaraOWP.model.Korisnik;

@Repository
public class KorisnikDaoImpl implements KorisnikDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class KorisnikRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String korisnickoIme = rs.getString(index++);
			String lozinka = rs.getString(index++);
			String eMail = rs.getString(index++);
			String ime = rs.getString(index++);
			String prezime = rs.getString(index++);
			String pol = rs.getString(index++);
			System.out.println("id " + id + "username" + korisnickoIme + "pass " +  lozinka);
			Boolean administrator = rs.getBoolean(index++);
			Date datumOd = rs.getDate(index++);
			Date datumDo = rs.getDate(index++);
			String adresa = rs.getString(index++);
			String broj = rs.getString(index++);
			Boolean blokiran  = rs.getBoolean(index++);
			
//			System.out.println(datumRegistracije);
//			
//			Date datum1 = java.sql.Date.valueOf(datum);
//			Date datum2 = java.sql.Date.valueOf(datumRegistracije);
			
			

			Korisnik korisnik = new Korisnik(id, korisnickoIme, lozinka, eMail, ime, prezime, pol, administrator , datumOd,datumDo, adresa, broj, blokiran);
			
			return korisnik;
		}
	}
	
	@Override
	public Korisnik findOne(String korisnickoIme) {
		try {
			String sql = "SELECT id, korisnickoIme,lozinka, eMail,ime, prezime, pol, administrator, datumRodjenja ,datumRegistracije, adresa, brTelefona, blokiran FROM korisnici WHERE korisnickoIme = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		try {
			String sql = "SELECT id,korisnickoIme,lozinka, eMail,ime, prezime, pol, administrator, datumRodjenja ,datumRegistracije, adresa, brTelefona, blokiran FROM korisnici WHERE korisnickoIme = ? AND lozinka = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme, lozinka);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public List<Korisnik> findAll() {
		String sql = "SELECT id, korisnickoIme, eMail,ime, prezime, pol, administrator,datumRegistracije, blokiran FROM korisnici";
		return jdbcTemplate.query(sql, new KorisnikRowMapper());
	}
	

	@Override
	public List<Korisnik> find(Long id,String korisnickoIme,String lozinka, String eMail,String ime,String prezime, String pol, Boolean administrator,Date datum, Date datumRegistracije,String adresa, String broj, Boolean blokiran) {
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT id, korisnickoIme,lozinka, eMail,ime, prezime, pol, administrator,datumRodjenja, datumRegistracije,adresa,brTelefona, blokiran FROM korisnici ";
		
		
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(korisnickoIme!=null) {
			korisnickoIme = "%" + korisnickoIme + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("korisnickoIme LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(korisnickoIme);
		}
		
		if(eMail!=null) {
			eMail = "%" + eMail + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("eMail LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(eMail);
		}
		
		if(ime!=null) {
			ime = "%" + ime + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("ime LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(ime);
		}
		
		if(prezime!=null) {
			prezime = "%" + prezime + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("prezime LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(prezime);
		}
		
		
		if(pol!=null) {
			pol = "%" + pol + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("pol LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(pol);
		}
		
		if(administrator!=null) {	
			//vraća samo administratore ili sve korisnike sistema
			String administratorSql = (administrator)? "administrator = 1": "administrator >= 0";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append(administratorSql);
			imaArgumenata = true;
		}
		
		if(datumRegistracije!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("datumRegistracije >= ?");
			imaArgumenata = true;
			listaArgumenata.add(datumRegistracije);
		}

		
		if(blokiran!=null) {	
			//vraća samo administratore ili sve korisnike sistema
			String blokiranSql = (blokiran)? "blokiran = 1": "blokiran >= 0";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append(blokiranSql);
			imaArgumenata = true;
		}
		
		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY korisnickoIme";
		else
			sql=sql;
		System.out.println(sql);
		
		return jdbcTemplate.query(sql, listaArgumenata.toArray(), new KorisnikRowMapper());
	}
	
	
	@Override
	public int save(Korisnik korisnik) {
		String sql = "INSERT INTO korisnici (korisnickoIme, lozinka, eMail,ime, prezime, pol, administrator, datumRodjenja,datumRegistracije, adresa, brTelefona, blokiran) VALUES (?, ?, ?,?,?, ?,?, ?,?,?,?, ?)";
		return jdbcTemplate.update(sql, korisnik.getKorisnickoIme(), korisnik.getLozinka(), korisnik.geteMail(), korisnik.getIme(), korisnik.getPrezime(),korisnik.getPol(), korisnik.isAdministrator(), korisnik.getDatum(), korisnik.getDatumRegistracije(),korisnik.getAdresa(), korisnik.getBroj(), korisnik.isBlokiran());
	}

	@Override
	public int update(Korisnik korisnik) {
		if (korisnik.getLozinka() == null) {
			String sql = "UPDATE korisnici SET eMail = ?, ime = ?, prezime = ?, pol = ?, administrator = ?,blokiran = ?  WHERE korisnickoIme = ?";
			return jdbcTemplate.update(sql, korisnik.geteMail(),korisnik.getIme(), korisnik.getPrezime(), korisnik.getPol(), korisnik.isAdministrator(),korisnik.isBlokiran(), korisnik.getKorisnickoIme());
		} else {
			String sql = "UPDATE korisnici SET lozinka = ?, eMail = ?, ime = ?, prezime = ?, pol = ?, administrator = ?, blokiran = ? WHERE korisnickoIme = ?";
			return jdbcTemplate.update(sql, korisnik.getLozinka(), korisnik.geteMail(),korisnik.getIme(), korisnik.getPrezime(), korisnik.getPol(), korisnik.isAdministrator(), korisnik.isBlokiran(),korisnik.getKorisnickoIme());
		}
	}
	@Override
	public int delete(String korisnickoIme) {
		String sql = "DELETE FROM korisnici WHERE korisnickoIme = ?";
		return jdbcTemplate.update(sql, korisnickoIme);
	}

	@Override
	public Korisnik findOne(long id) {
		try {
			String sql = "SELECT id, korisnickoIme,lozinka, eMail, ime, prezime, pol, administrator, datumRodjenja , datumRegistracije, adresa, brTelefona, blokiran FROM korisnici WHERE id = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
}
