package com.ftn.KnjizaraOWP.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.KnjizaraOWP.dao.KorisnikDao;
import com.ftn.KnjizaraOWP.dao.LoyaltyKarticaDAO;
import com.ftn.KnjizaraOWP.model.Korisnik;
import com.ftn.KnjizaraOWP.model.LoyaltyKartica;

@Repository
public class LoyaltyKarticaDAOImpl implements LoyaltyKarticaDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KorisnikDao korisnikDAO;
	
	private class LoyaltyKarticeRowMapper implements RowMapper<LoyaltyKartica>{

		@Override
		public LoyaltyKartica mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int id=rs.getInt(index++);
			int popust = rs.getInt(index++);
			int brPoena = rs.getInt(index++);
			int vlasnikOznaka = rs.getInt(index++);
			Korisnik vlasnik = (Korisnik)korisnikDAO.findOne(vlasnikOznaka);
			boolean status = rs.getBoolean(index++);
			
			
			LoyaltyKartica loyaltyKartica = new LoyaltyKartica(id, popust, brPoena, vlasnik, status);
			return loyaltyKartica;
			
		}
		
		
		
	}


	@Override
	public LoyaltyKartica find(int id) {
		try {
			String sql = "SELECT * FROM loyaltykartice where id = ?";
			return jdbcTemplate.queryForObject(sql, new LoyaltyKarticeRowMapper(), id);
		} catch(EmptyResultDataAccessException ex) {
			return null;
		}

	}

	@Override
	public LoyaltyKartica findOne(int vlasnikOznaka) {
		try {
			String sql = "SELECT * FROM loyaltykartice where vlasnikOznaka = ?";
			return jdbcTemplate.queryForObject(sql, new LoyaltyKarticeRowMapper(), vlasnikOznaka);
		} catch(EmptyResultDataAccessException ex) {
			return null;
		}
	}


	@Override
	public List<LoyaltyKartica> findAll() {
		String sql = "SELECT id, popust, brPoena, vlasnikOznaka, status from loyaltykartice";
		return jdbcTemplate.query(sql, new LoyaltyKarticeRowMapper());

	}

	@Override
	public List<LoyaltyKartica> find(int id, String popust, int brPoena, String vlasnikOznaka, String status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LoyaltyKartica> findVerzija2(HashMap<String, Object> mapaArgumenata) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(LoyaltyKartica loyaltyKartica) {
		String sql = "INSERT INTO loyaltykartice(popust, brPoena, vlasnikOznaka, status) VALUES (?,?,?,?)";
		jdbcTemplate.update(sql, loyaltyKartica.getPopust(), loyaltyKartica.getBrPoena(), loyaltyKartica.getVlasnik().getId(), loyaltyKartica.getStatus());

		
	}

	@Override
	public void update(LoyaltyKartica loyaltyKartica) {
		String sql = "UPDATE loyaltykartice SET popust=?, brPoena=?, vlasnikOznaka=?, status=? WHERE id=?";
		jdbcTemplate.update(sql, loyaltyKartica.getPopust(), loyaltyKartica.getBrPoena(), loyaltyKartica.getVlasnik().getId(), loyaltyKartica.getStatus(), loyaltyKartica.getId());
		
	}

	@Override
	public int delete(int id) {
		String sql = "DELETE FROM loyaltykartice WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

	@Override
	public List<LoyaltyKartica> findAll2() {
		String sql = "SELECT * FROM loyaltykartice where status=0";
		return jdbcTemplate.query(sql, new LoyaltyKarticeRowMapper());
	}
	


}
