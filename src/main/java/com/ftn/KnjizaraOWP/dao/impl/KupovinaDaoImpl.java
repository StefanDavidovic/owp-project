package com.ftn.KnjizaraOWP.dao.impl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.KnjizaraOWP.dao.KnjigaDao;
import com.ftn.KnjizaraOWP.dao.KorisnikDao;
import com.ftn.KnjizaraOWP.dao.KupovinaDao;
import com.ftn.KnjizaraOWP.model.Knjiga;
import com.ftn.KnjizaraOWP.model.Korisnik;
import com.ftn.KnjizaraOWP.model.Kupovina;

@Repository
public class KupovinaDaoImpl implements KupovinaDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KnjigaDao knjigaDAO;
	
	@Autowired
	private KorisnikDao korisnikDAO;

	
	private class KupovinaRowMapper implements RowMapper<Kupovina> {

		@Override
		public Kupovina mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int id = rs.getInt(index++);
			System.out.println(id + "KUPOVINAAA ID");
			Long knjigaId = rs.getLong(index++);
			System.out.println(knjigaId);
			Knjiga knjiga = knjigaDAO.findOne(knjigaId);
			BigDecimal ukupnaCena = rs.getBigDecimal(index++);
			System.out.println(ukupnaCena);
			LocalDate datumOd = rs.getDate(index++).toLocalDate();
			int korisnickoIme = rs.getInt(index++);
			Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
			int ukupanBrojKnjiga = rs.getInt(index++);
			

			
			Kupovina kupovine = new Kupovina(id, knjiga, ukupnaCena, datumOd, korisnik, ukupanBrojKnjiga);
			return kupovine;
		}

	}

	@Override
	public Kupovina findOne(int id) {
		String sql = 
				"SELECT id, knjigaId, ukupnaCena, datumKupovine, korisnikId, brojKupljenihKnjiga FROM knjizara.kupovina WHERE id = ? ";
		return jdbcTemplate.queryForObject(sql, new KupovinaRowMapper(), id);
	}

	@Override
	public Kupovina findOne(LocalDate datumKupovine) {
		try {
			String sql = "SELECT * FROM kupovina where datumKupovine = ?";
			return jdbcTemplate.queryForObject(sql, new KupovinaRowMapper(), datumKupovine);
		} catch(EmptyResultDataAccessException ex) {
			return null;
		}
	}


	@Override
	public List<Kupovina> findAll() {
		String sql = 
				"SELECT * FROM knjizara.kupovina;";
		return jdbcTemplate.query(sql, new KupovinaRowMapper());
	}
	
	@Override
	public List<Kupovina> findAll2() {
		String sql = 
				"SELECT id, knjigaId,  ukupnaCena, datumKupovine, korisnikId,   count(brojKupljenihKnjiga) brKnjiga FROM knjizara.kupovina GROUP BY knjigaId;";
		return jdbcTemplate.query(sql, new KupovinaRowMapper());
	}

	@Override
	public List<Kupovina> find(int id, String musterijaOznaka) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Kupovina> find(int knjigaId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Kupovina> findd(Long musterijaOznaka) {
		String sql = 
				"SELECT * FROM knjizara.kupovina WHERE korisnikId = ? ORDER BY datumKupovine desc";
		return jdbcTemplate.query(sql, new KupovinaRowMapper(), musterijaOznaka);
	}

	@Override
	public void save(Kupovina kupovina) {
		String sql = "INSERT INTO kupovina(knjigaId, ukupnaCena, datumKupovine, korisnikId, brojKupljenihKnjiga) VALUES (?,?,?,?,?)";
		jdbcTemplate.update(sql, kupovina.getKnjiga().getId(), kupovina.getUkupnaCena(), kupovina.getDatumKupovine() , kupovina.getMusterija().getId(), kupovina.getBrojKupljenihKnjiga());		
	}

		
	

	@Override
	public int update(Kupovina kupovina) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Kupovina> find(HashMap<String, Object> mapaArgumenata) {
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT id,knjigaId,ukupnaCena,datumKupovine,korisnikId,brojKupljenihKnjiga from kupovina"; 
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
				
		if(mapaArgumenata.containsKey("datumKupovineOd")) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("datumKupovine >= ?");
			imaArgumenata = true;
			listaArgumenata.add(mapaArgumenata.get("datumKupovineOd"));
		}
		
		if(mapaArgumenata.containsKey("datumKupovineDo")) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("datumKupovine <= ?");
			imaArgumenata = true;
			listaArgumenata.add(mapaArgumenata.get("datumKupovineDo"));
			
			
		}
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY id";
		else
			sql=sql + " ORDER BY id";
		System.out.println(sql);
		
		return jdbcTemplate.query(sql, new KupovinaRowMapper(), listaArgumenata.toArray());
		
	}


}
