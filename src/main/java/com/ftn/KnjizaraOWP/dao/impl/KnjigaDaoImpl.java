package com.ftn.KnjizaraOWP.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.KnjizaraOWP.dao.KnjigaDao;
import com.ftn.KnjizaraOWP.model.EPismo;
import com.ftn.KnjizaraOWP.model.EPovez;
import com.ftn.KnjizaraOWP.model.Knjiga;

@Repository
public class KnjigaDaoImpl implements KnjigaDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class KnjigaRowMapper implements RowMapper<Knjiga> {
	
		@Override
		public Knjiga mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String naziv = rs.getString(index++);
			String isbn = rs.getString(index++);
			String izdavackaKuca = rs.getString(index++);
			String autor = rs.getString(index++);
			String opis = rs.getString(index++);
			BigDecimal cena = rs.getBigDecimal(index++);
			int brStrana = rs.getInt(index++);
			int kolicina = rs.getInt(index++);
			String povez = rs.getString(index++);
			String pismo = rs.getString(index++);
			String jezik = rs.getString(index++);
			double prosecnaOcena = rs.getDouble(index++);
			String slika = rs.getString(index++);
			Date datumOd = rs.getDate(index++);
			Date datumDo = rs.getDate(index++);
			Integer popust = rs.getInt(index++);
			
//			double prosecnaOcena = 1.0;



			Knjiga knjiga = new Knjiga(id, naziv, isbn ,izdavackaKuca, autor, opis, cena, brStrana,kolicina, povez, pismo,jezik, prosecnaOcena, slika, datumOd, datumDo, popust);
			return knjiga;
		}


	}

	@Override
	public Knjiga findOne(long id) {
		String sql = "SELECT k.id, k.naziv, k.isbn, k.izdavackaKuca, k.autor, k.opis, k.cena, k.brStrana,k.kolicina, k.povez, k.pismo, k.jezikPisanja, k.prosecnaOcena,k.slika, k.datumOd, k.datumDo, k.popust\r\n"
				+ "FROM knjizara.knjige k\r\n"
				+ "WHERE k.id = ?";

		return  jdbcTemplate.queryForObject(sql, new KnjigaRowMapper(), id);

	}

	@Override
	public List<Knjiga> findAll() {
		System.out.println("NALAZIIII");
		String sql ="SELECT k.id, k.naziv, k.isbn, k.izdavackaKuca, k.autor, k.opis, k.cena, k.brStrana,k.kolicina, k.povez, k.pismo, k.jezikPisanja, k.prosecnaOcena, k.slika, k.datumOd, k.datumDo, k.popust\r\n"
				+ "FROM knjizara.knjige k";		

		return jdbcTemplate.query(sql, new KnjigaRowMapper());
	}

	@Override
	public List<Knjiga> find(Long id,String naziv, String isbn, String izdavackaKuca, String autor,String opis, String jezik, Double cenaOd, Double cenaDo, Double prosecnaOcena, Integer brStrana,Integer kolicina, String povez, String pismo, String slika, Date datumOd, Date datumDo, Integer popust) {

		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT k.id, k.naziv, k.isbn, k.izdavackaKuca, k.autor, k.opis, k.cena, k.brStrana,k.kolicina, k.povez, k.pismo, k.jezikPisanja, k.prosecnaOcena, k.slika, k.datumOd, k.datumDo, k.popust\r\n"
				+ "FROM knjizara.knjige k";
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(naziv != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.naziv = ?");
			imaArgumenata = true;
			listaArgumenata.add(naziv);
		}
		
		if(isbn != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.isbn = ?");
			imaArgumenata = true;
			listaArgumenata.add(isbn);
		}
		
		if(izdavackaKuca != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.izdavackaKuca = ?");
			imaArgumenata = true;
			listaArgumenata.add(izdavackaKuca);
		}
		
		if(autor != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.autor = ?");
			imaArgumenata = true;
			listaArgumenata.add(autor);
		}
		
		if(jezik != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.jezik = ?");
			imaArgumenata = true;
			listaArgumenata.add(jezik);
		}
		
		if(cenaOd!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.cena >= ?");
			imaArgumenata = true;
			listaArgumenata.add(cenaOd);
		}
		
		if(cenaDo!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.cena <= ?");
			imaArgumenata = true;
			listaArgumenata.add(cenaDo);
		}
		
		if(prosecnaOcena != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.prosecnaOcena = ?");
			imaArgumenata = true;
			listaArgumenata.add(prosecnaOcena);
		}
		
		if(brStrana != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.brStrana = ?");
			imaArgumenata = true;
			listaArgumenata.add(brStrana);
		}
		
		if(povez != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.povez = ?");
			imaArgumenata = true;
			listaArgumenata.add(povez);
		}
		
		if(pismo != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.pismo = ?");
			imaArgumenata = true;
			listaArgumenata.add(pismo);
		}
		
		if(imaArgumenata)
			sql=sql + whereSql.toString();
		else
			sql=sql + " ORDER BY k.id";
		System.out.println(sql);
		
		return jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaRowMapper());
	}

//	@Transactional
	@Override
	public int save(Knjiga knjiga) {
		System.out.println("DAAAAOOOO");
		String sql = "INSERT INTO knjige (naziv, isbn, izdavackaKuca, autor, opis, cena, brStrana,kolicina, povez, pismo, jezikPisanja, prosecnaOcena, slika, datumOd, datumDo, popust) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		return jdbcTemplate.update(sql, knjiga.getNaziv(), knjiga.getIsbn(), knjiga.getIzdavackaKuca(), knjiga.getAutor(), knjiga.getOpis() ,knjiga.getCena(),knjiga.getBrStrana(), knjiga.getKolicina() ,knjiga.getPovez(),knjiga.getPismo(),knjiga.getJezik(),knjiga.getProsecnaOcena(), knjiga.getSlika(), knjiga.getDatumOd(), knjiga.getDatumDo(), knjiga.getPopust());
	}


	@Override
	public int update(Knjiga knjiga) {
		String sql = "UPDATE knjige SET naziv = ?, isbn = ?, izdavackaKuca = ?, autor = ?, opis = ?, cena = ?, brStrana = ?, kolicina = ? ,povez = ?, pismo = ?, jezikPisanja = ?, prosecnaOcena = ?, slika = ?, datumOd = ?, datumDo = ?, popust = ? WHERE id  = ?";
		return jdbcTemplate.update(sql, knjiga.getNaziv(), knjiga.getIsbn(), knjiga.getIzdavackaKuca(), knjiga.getAutor(), knjiga.getOpis() ,knjiga.getCena(),knjiga.getBrStrana(), knjiga.getKolicina() ,knjiga.getPovez(),knjiga.getPismo(),knjiga.getJezik(),knjiga.getProsecnaOcena(),knjiga.getSlika(), knjiga.getDatumOd(), knjiga.getDatumDo(), knjiga.getPopust(), knjiga.getId());

	}

	@Override
	public int delete(long id) {
		String sql = "DELETE FROM knjige WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

	@Override
	public List<Knjiga> sortirajPoNazivu() {
		String sql ="SELECT k.id, k.naziv, k.isbn, k.izdavackaKuca, k.autor, k.opis, k.cena, k.brStrana,k.kolicina, k.povez, k.pismo, k.jezikPisanja, k.prosecnaOcena, k.slika, k.datumOd, k.datumDo, k.popust\r\n"
				+ "FROM knjizara.knjige k\r\n"
				+ "ORDER BY naziv asc, cena asc";

		return jdbcTemplate.query(sql, new KnjigaRowMapper());
	}



}
