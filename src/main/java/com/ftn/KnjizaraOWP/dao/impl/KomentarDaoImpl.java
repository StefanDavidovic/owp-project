package com.ftn.KnjizaraOWP.dao.impl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.KnjizaraOWP.dao.KnjigaDao;
import com.ftn.KnjizaraOWP.dao.KomentarDAO;
import com.ftn.KnjizaraOWP.dao.KorisnikDao;
import com.ftn.KnjizaraOWP.model.Knjiga;
import com.ftn.KnjizaraOWP.model.Komentar;
import com.ftn.KnjizaraOWP.model.Korisnik;
import com.ftn.KnjizaraOWP.model.Kupovina;
import com.ftn.KnjizaraOWP.model.StatusKomentara;

@Repository
public class KomentarDaoImpl implements KomentarDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KnjigaDao knjigaDAO;
	
	@Autowired
	private KorisnikDao korisnikDAO;
	
	private class KomentarRowMapper implements RowMapper<Komentar> {
		
		@Override
		public Komentar mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
            Long komentarId = rs.getLong(index++);
            Integer ocena = rs.getInt(index++);
            Date datum = rs.getDate(index++);
            Integer korisnikId = rs.getInt(index++);
            Korisnik korisnik = korisnikDAO.findOne(korisnikId);
            
            Long knjigaId = rs.getLong(index++);
            Knjiga knjiga = knjigaDAO.findOne(knjigaId);
            
            StatusKomentara status = StatusKomentara.valueOf(rs.getString(index++));
            String opis = rs.getString(index++);
          
            

            Komentar komentari = new Komentar(komentarId, opis, ocena, datum, korisnik, knjiga, status);
            return komentari;

		}

	}

	@Override
	public Komentar findOne(Long id) {
		String sql = "SELECT * FROM knjizara.komentar WHERE id = ?";

		return  jdbcTemplate.queryForObject(sql, new KomentarRowMapper(), id);
	}

	@Override
	public List<Komentar> findAll() {
		String sql ="SELECT * FROM knjizara.komentar";	

		return jdbcTemplate.query(sql, new KomentarRowMapper());
	}

	@Override
	public Komentar izvuciOdKorisnika(String korisnicko) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int save(Komentar komentar) {
		String sql = "INSERT INTO komentar (ocena,datum,korisnik,knjigaId,statusKomentara,opis) VALUES (?,?, ?,?,?,?)";
		return jdbcTemplate.update(sql,komentar.getOcena(),komentar.getVremeKomentara(),komentar.getKorisnik().getId(),komentar.getKnjiga().getId(),komentar.getStatus().toString(),komentar.getTekstKomentara());
	}

	@Override
	public int update(Komentar komentar) {
		String sql = "UPDATE komentar SET ocena = ?, datum = ?, korisnik = ?,knjigaId = ?,statusKomentara = ?,opis = ? where id = ? ";
		return jdbcTemplate.update(sql,komentar.getOcena(),komentar.getVremeKomentara(),komentar.getKorisnik().getId(),komentar.getKnjiga().getId(),komentar.getStatus().toString(),komentar.getTekstKomentara(),komentar.getId());
	}

	@Override
	public int delete(Long id) {
		// TODO Auto-generated method stub
		return 0;
	}

}
