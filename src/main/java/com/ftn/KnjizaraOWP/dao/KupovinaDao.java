package com.ftn.KnjizaraOWP.dao;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import com.ftn.KnjizaraOWP.model.Kupovina;

public interface KupovinaDao {
	
	public Kupovina findOne(int id);
	
	public Kupovina findOne(LocalDate datumKupovine);

	
	public List<Kupovina> findAll();
	
	public List<Kupovina> findAll2();
		
	public List<Kupovina> find(int id, String musterijaOznaka);
	
	public List<Kupovina> find(int knjigaId);
	
	public List<Kupovina> findd(Long musterijaOznaka);

	public void save(Kupovina kupovina);
	
	public int update(Kupovina kupovina);
	
	public void delete(Integer id);
	
	public List<Kupovina> find(HashMap<String, Object> mapaArgumenata);


}
