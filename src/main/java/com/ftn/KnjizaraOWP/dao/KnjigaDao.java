package com.ftn.KnjizaraOWP.dao;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.ftn.KnjizaraOWP.model.EPismo;
import com.ftn.KnjizaraOWP.model.EPovez;
import com.ftn.KnjizaraOWP.model.Knjiga;

public interface KnjigaDao {

	
	public Knjiga findOne(long id);

	public List<Knjiga> findAll();

	public List<Knjiga> find(Long id, String naziv, String isbn, String izdavackaKuca, String autor,String opis, String jezik, Double cenaOd, Double cenaDo, Double prosecnaOcena, Integer brStrana,Integer kolicina, String povez, String pismo, String slika, Date datumOd, Date datumDo, Integer popust);
	
	public int save(Knjiga knjiga);

	public int update(Knjiga knjiga);

	public int delete(long id);
	
	public List<Knjiga> sortirajPoNazivu();
}
