package com.ftn.KnjizaraOWP.dao;

import java.util.HashMap;
import java.util.List;

import com.ftn.KnjizaraOWP.model.LoyaltyKartica;

public interface LoyaltyKarticaDAO {
	public LoyaltyKartica find(int id);
	
	public LoyaltyKartica findOne(int vlasnikOznaka);
	
	public List<LoyaltyKartica> findAll();
	
	public List<LoyaltyKartica> findAll2();
	
	public List<LoyaltyKartica> find(int id, String popust, int brPoena,
			String vlasnikOznaka, String status);
	

	public List<LoyaltyKartica> findVerzija2(HashMap<String, Object> mapaArgumenata);

	
	
	public void save(LoyaltyKartica loyaltyKartica);
	
	public void update(LoyaltyKartica loyaltyKartica);
	
	public int delete(int id);

}
