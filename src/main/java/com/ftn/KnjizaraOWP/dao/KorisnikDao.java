package com.ftn.KnjizaraOWP.dao;

import java.util.Date;
import java.util.List;

import com.ftn.KnjizaraOWP.model.Korisnik;


public interface KorisnikDao {
	
	public Korisnik findOne(String korisnickoIme);
	
	public Korisnik findOne(long id);

	public Korisnik findOne(String korisnickoIme, String lozinka);

	public List<Korisnik> findAll();

	public List<Korisnik> find(Long id,String korisnickoIme,String lozinka, String eMail,String ime,String prezime, String pol, Boolean administrator,Date datum, Date datumRegistracije,String adresa, String broj, Boolean blokiran);
	
	public int save(Korisnik korisnik);

	public int update(Korisnik korisnik);

	public int delete(String korisnickoIme);

}
