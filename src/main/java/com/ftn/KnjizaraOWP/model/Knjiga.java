package com.ftn.KnjizaraOWP.model;

import java.beans.Transient;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Knjiga {

	private long id;
	private String naziv, isbn, izdavackaKuca, autor, opis, jezik;
	private Double prosecnaOcena;
	private BigDecimal cena;
	private Integer brStrana, kolicina, popust=1;
	private String povez, pismo, slika;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date datumOd;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date datumDo;

	public Knjiga(long id, String naziv, String isbn, String izdavackaKuca, String autor, String opis, BigDecimal cena,
			Integer brStrana, Integer kolicina, String povez, String pismo, String jezik, Double prosecnaOcena,
			String slika, Date datumOd, Date datumDo, Integer popust) {
		super();

		this.id = id;
		this.naziv = naziv;
		this.isbn = isbn;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.opis = opis;
		this.jezik = jezik;
		this.cena = cena;
		this.prosecnaOcena = prosecnaOcena;
		this.brStrana = brStrana;
		this.kolicina = kolicina;
		this.povez = povez;
		this.pismo = pismo;
		this.slika = slika;
		this.datumOd = datumOd;
		this.datumDo  = datumDo;
		this.popust = popust;
	}

	public Knjiga() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getIzdavackaKuca() {
		return izdavackaKuca;
	}

	public void setIzdavackaKuca(String izdavackaKuca) {
		this.izdavackaKuca = izdavackaKuca;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getJezik() {
		return jezik;
	}

	public void setJezik(String jezik) {
		this.jezik = jezik;
	}

	public Double getProsecnaOcena() {
		return prosecnaOcena;
	}

	public void setProsecnaOcena(Double prosecnaOcena) {
		this.prosecnaOcena = prosecnaOcena;
	}

	public BigDecimal getCena() {
		return cena;
	}

	public void setCena(BigDecimal cena) {
		this.cena = cena;
	}

	public Integer getBrStrana() {
		return brStrana;
	}

	public void setBrStrana(Integer brStrana) {
		this.brStrana = brStrana;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Integer getPopust() {
		return popust;
	}

	public void setPopust(Integer popust) {
		this.popust = popust;
	}

	public String getPovez() {
		return povez;
	}

	public void setPovez(String povez) {
		this.povez = povez;
	}

	public String getPismo() {
		return pismo;
	}

	public void setPismo(String pismo) {
		this.pismo = pismo;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public Date getDatumOd() {
		return datumOd;
	}

	public void setDatumOd(Date datumOd) {
		this.datumOd = datumOd;
	}

	public Date getDatumDo() {
		return datumDo;
	}

	public void setDatumDo(Date datumDo) {
		this.datumDo = datumDo;
	}

	@Override
	public String toString() {
		return "Knjiga [id=" + id + ", naziv=" + naziv + ", isbn=" + isbn + ", izdavackaKuca=" + izdavackaKuca
				+ ", autor=" + autor + ", opis=" + opis + ", jezik=" + jezik + ", prosecnaOcena=" + prosecnaOcena
				+ ", cena=" + cena + ", brStrana=" + brStrana + ", kolicina=" + kolicina + ", popust=" + popust
				+ ", povez=" + povez + ", pismo=" + pismo + ", slika=" + slika + ", datumOd=" + datumOd + ", datumDo="
				+ datumDo + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((autor == null) ? 0 : autor.hashCode());
		result = prime * result + ((brStrana == null) ? 0 : brStrana.hashCode());
		result = prime * result + ((cena == null) ? 0 : cena.hashCode());
		result = prime * result + ((datumDo == null) ? 0 : datumDo.hashCode());
		result = prime * result + ((datumOd == null) ? 0 : datumOd.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		result = prime * result + ((izdavackaKuca == null) ? 0 : izdavackaKuca.hashCode());
		result = prime * result + ((jezik == null) ? 0 : jezik.hashCode());
		result = prime * result + ((kolicina == null) ? 0 : kolicina.hashCode());
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + ((pismo == null) ? 0 : pismo.hashCode());
		result = prime * result + ((popust == null) ? 0 : popust.hashCode());
		result = prime * result + ((povez == null) ? 0 : povez.hashCode());
		result = prime * result + ((prosecnaOcena == null) ? 0 : prosecnaOcena.hashCode());
		result = prime * result + ((slika == null) ? 0 : slika.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Knjiga other = (Knjiga) obj;
		if (autor == null) {
			if (other.autor != null)
				return false;
		} else if (!autor.equals(other.autor))
			return false;
		if (brStrana == null) {
			if (other.brStrana != null)
				return false;
		} else if (!brStrana.equals(other.brStrana))
			return false;
		if (cena == null) {
			if (other.cena != null)
				return false;
		} else if (!cena.equals(other.cena))
			return false;
		if (datumDo == null) {
			if (other.datumDo != null)
				return false;
		} else if (!datumDo.equals(other.datumDo))
			return false;
		if (datumOd == null) {
			if (other.datumOd != null)
				return false;
		} else if (!datumOd.equals(other.datumOd))
			return false;
		if (id != other.id)
			return false;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		if (izdavackaKuca == null) {
			if (other.izdavackaKuca != null)
				return false;
		} else if (!izdavackaKuca.equals(other.izdavackaKuca))
			return false;
		if (jezik == null) {
			if (other.jezik != null)
				return false;
		} else if (!jezik.equals(other.jezik))
			return false;
		if (kolicina == null) {
			if (other.kolicina != null)
				return false;
		} else if (!kolicina.equals(other.kolicina))
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		if (opis == null) {
			if (other.opis != null)
				return false;
		} else if (!opis.equals(other.opis))
			return false;
		if (pismo == null) {
			if (other.pismo != null)
				return false;
		} else if (!pismo.equals(other.pismo))
			return false;
		if (popust == null) {
			if (other.popust != null)
				return false;
		} else if (!popust.equals(other.popust))
			return false;
		if (povez == null) {
			if (other.povez != null)
				return false;
		} else if (!povez.equals(other.povez))
			return false;
		if (prosecnaOcena == null) {
			if (other.prosecnaOcena != null)
				return false;
		} else if (!prosecnaOcena.equals(other.prosecnaOcena))
			return false;
		if (slika == null) {
			if (other.slika != null)
				return false;
		} else if (!slika.equals(other.slika))
			return false;
		return true;
	}
	
	

	
}