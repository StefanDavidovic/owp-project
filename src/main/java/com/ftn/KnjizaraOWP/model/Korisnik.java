package com.ftn.KnjizaraOWP.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Korisnik {
	long id;
	private String korisnickoIme="", lozinka="", eMail="", pol="muški", adresa="", broj="", ime="", prezime = "";
	private boolean administrator = false;
	private Date datum = new Date();
	private Date datumRegistracije = new Date();
	private boolean blokiran = false;
	
	public Korisnik() {
	}
	
	public Korisnik(String korisnickoIme, String lozinka, String email,String ime, String prezime, String pol, boolean administrator, Date datum,Date datumRegistracije, String adresa, String broj, boolean blokiran) {
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.eMail = email;
		this.ime = ime;
		this.prezime = prezime;
		this.pol = pol;
		this.administrator = administrator;
		this.datum = datum;
		this.datumRegistracije= datumRegistracije; 
		this.adresa = adresa;
		this.broj = broj;
		this.blokiran = blokiran;
		
	}
	
	public Korisnik(Long id,String korisnickoIme, String lozinka, String email,String ime, String prezime, String pol, boolean administrator, Date datum,Date datumRegistracije, String adresa, String broj, boolean blokiran) {
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.eMail = email;
		this.ime = ime;
		this.prezime = prezime;
		this.pol = pol;
		this.administrator = administrator;
		this.datum = datum;
		this.datumRegistracije = datumRegistracije;
		this.adresa = adresa;
		this.broj = broj;
		this.blokiran = blokiran;
	}
	
	

	public Date getDatumRegistracije() {
		return datumRegistracije;
	}

	public void setDatumRegistracije(Date datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public boolean isAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public boolean isBlokiran() {
		return blokiran;
	}

	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}

	@Override
	public String toString() {
		return "Korisnik [id=" + id + ", korisnickoIme=" + korisnickoIme + ", lozinka=" + lozinka + ", eMail=" + eMail
				+ ", pol=" + pol + ", adresa=" + adresa + ", broj=" + broj + ", ime=" + ime + ", prezime=" + prezime
				+ ", administrator=" + administrator + ", datum=" + datum + ", datumRegistracije=" + datumRegistracije
				+ ", blokiran=" + blokiran + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (administrator ? 1231 : 1237);
		result = prime * result + ((adresa == null) ? 0 : adresa.hashCode());
		result = prime * result + (blokiran ? 1231 : 1237);
		result = prime * result + ((broj == null) ? 0 : broj.hashCode());
		result = prime * result + ((datum == null) ? 0 : datum.hashCode());
		result = prime * result + ((datumRegistracije == null) ? 0 : datumRegistracije.hashCode());
		result = prime * result + ((eMail == null) ? 0 : eMail.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((ime == null) ? 0 : ime.hashCode());
		result = prime * result + ((korisnickoIme == null) ? 0 : korisnickoIme.hashCode());
		result = prime * result + ((lozinka == null) ? 0 : lozinka.hashCode());
		result = prime * result + ((pol == null) ? 0 : pol.hashCode());
		result = prime * result + ((prezime == null) ? 0 : prezime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Korisnik other = (Korisnik) obj;
		if (administrator != other.administrator)
			return false;
		if (adresa == null) {
			if (other.adresa != null)
				return false;
		} else if (!adresa.equals(other.adresa))
			return false;
		if (blokiran != other.blokiran)
			return false;
		if (broj == null) {
			if (other.broj != null)
				return false;
		} else if (!broj.equals(other.broj))
			return false;
		if (datum == null) {
			if (other.datum != null)
				return false;
		} else if (!datum.equals(other.datum))
			return false;
		if (datumRegistracije == null) {
			if (other.datumRegistracije != null)
				return false;
		} else if (!datumRegistracije.equals(other.datumRegistracije))
			return false;
		if (eMail == null) {
			if (other.eMail != null)
				return false;
		} else if (!eMail.equals(other.eMail))
			return false;
		if (id != other.id)
			return false;
		if (ime == null) {
			if (other.ime != null)
				return false;
		} else if (!ime.equals(other.ime))
			return false;
		if (korisnickoIme == null) {
			if (other.korisnickoIme != null)
				return false;
		} else if (!korisnickoIme.equals(other.korisnickoIme))
			return false;
		if (lozinka == null) {
			if (other.lozinka != null)
				return false;
		} else if (!lozinka.equals(other.lozinka))
			return false;
		if (pol == null) {
			if (other.pol != null)
				return false;
		} else if (!pol.equals(other.pol))
			return false;
		if (prezime == null) {
			if (other.prezime != null)
				return false;
		} else if (!prezime.equals(other.prezime))
			return false;
		return true;
	}







	

}
