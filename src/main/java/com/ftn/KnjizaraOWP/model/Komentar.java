package com.ftn.KnjizaraOWP.model;

import java.util.Date;

public class Komentar {
	
	private Long id;
	private String tekstKomentara;
	private double ocena;
	private Date vremeKomentara;
	private Korisnik korisnik;
	private Knjiga knjiga;
	private StatusKomentara status;
	
	



	
	
	public Komentar(Long id, String tekstKomentara, double ocena, Date vremeKomentara, Korisnik korisnik,
			Knjiga knjiga, StatusKomentara status) {
		super();
		this.id = id;
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.vremeKomentara = vremeKomentara;
		this.korisnik = korisnik;
		this.knjiga = knjiga;
		this.status = status;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getTekstKomentara() {
		return tekstKomentara;
	}

	public void setTekstKomentara(String tekstKomentara) {
		this.tekstKomentara = tekstKomentara;
	}


	public double getOcena() {
		return ocena;
	}

	public void setOcena(double ocena) {
		this.ocena = ocena;
	}

	public Date getVremeKomentara() {
		return vremeKomentara;
	}


	public void setVremeKomentara(Date vremeKomentara) {
		this.vremeKomentara = vremeKomentara;
	}


	public Korisnik getKorisnik() {
		return korisnik;
	}


	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}


	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}


	public StatusKomentara getStatus() {
		return status;
	}

	public void setStatus(StatusKomentara status) {
		this.status = status;
	}


	@Override
	public String toString() {
		return "Komentar [id=" + id + ", tekstKomentara=" + tekstKomentara + ", ocena=" + ocena + ", vremeKomentara="
				+ vremeKomentara + ", korisnik=" + korisnik + ", knjiga=" + knjiga + ", status=" + status + "]";
	}







	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((knjiga == null) ? 0 : knjiga.hashCode());
		result = prime * result + ((korisnik == null) ? 0 : korisnik.hashCode());
		long temp;
		temp = Double.doubleToLongBits(ocena);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tekstKomentara == null) ? 0 : tekstKomentara.hashCode());
		result = prime * result + ((vremeKomentara == null) ? 0 : vremeKomentara.hashCode());
		return result;
	}







	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Komentar other = (Komentar) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (knjiga == null) {
			if (other.knjiga != null)
				return false;
		} else if (!knjiga.equals(other.knjiga))
			return false;
		if (korisnik == null) {
			if (other.korisnik != null)
				return false;
		} else if (!korisnik.equals(other.korisnik))
			return false;
		if (Double.doubleToLongBits(ocena) != Double.doubleToLongBits(other.ocena))
			return false;
		if (status != other.status)
			return false;
		if (tekstKomentara == null) {
			if (other.tekstKomentara != null)
				return false;
		} else if (!tekstKomentara.equals(other.tekstKomentara))
			return false;
		if (vremeKomentara == null) {
			if (other.vremeKomentara != null)
				return false;
		} else if (!vremeKomentara.equals(other.vremeKomentara))
			return false;
		return true;
	}
	
	

}
