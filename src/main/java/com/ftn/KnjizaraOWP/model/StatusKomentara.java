package com.ftn.KnjizaraOWP.model;

public enum StatusKomentara {
	
	naCekanju,
	odobren,
	nijeOdobren

}
