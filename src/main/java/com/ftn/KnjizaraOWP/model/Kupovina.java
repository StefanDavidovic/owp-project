package com.ftn.KnjizaraOWP.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Kupovina {
	
	private int id;
	private Knjiga knjiga;
	private BigDecimal ukupnaCena;
	private LocalDate datumKupovine;
	private Korisnik musterija;
	private int brojKupljenihKnjiga;

	
	public Kupovina() {
	}


	public Kupovina(Knjiga knjiga, BigDecimal ukupnaCena, LocalDate datumKupovine, Korisnik musterija,
			int brojKupljenihKnjiga) {
		this.knjiga = knjiga;
		this.ukupnaCena = ukupnaCena;
		this.datumKupovine = datumKupovine;
		this.musterija = musterija;
		this.brojKupljenihKnjiga = brojKupljenihKnjiga;
	}
	
	public Kupovina(int id, Knjiga knjiga, BigDecimal ukupnaCena, LocalDate datumKupovine, Korisnik musterija,
			int brojKupljenihKnjiga) {
		this.id = id;
		this.knjiga = knjiga;
		this.ukupnaCena = ukupnaCena;
		this.datumKupovine = datumKupovine;
		this.musterija = musterija;
		this.brojKupljenihKnjiga = brojKupljenihKnjiga;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Knjiga getKnjiga() {
		return knjiga;
	}


	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}


	public BigDecimal getUkupnaCena() {
		return ukupnaCena;
	}


	public void setUkupnaCena(BigDecimal ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}


	public LocalDate getDatumKupovine() {
		return datumKupovine;
	}


	public void setDatumKupovine(LocalDate datumKupovine) {
		this.datumKupovine = datumKupovine;
	}


	public Korisnik getMusterija() {
		return musterija;
	}


	public void setMusterija(Korisnik musterija) {
		this.musterija = musterija;
	}


	public int getBrojKupljenihKnjiga() {
		return brojKupljenihKnjiga;
	}


	public void setBrojKupljenihKnjiga(int brojKupljenihKnjiga) {
		this.brojKupljenihKnjiga = brojKupljenihKnjiga;
	}


	@Override
	public String toString() {
		return "Kupovina [id=" + id + ", knjiga=" + knjiga + ", ukupnaCena=" + ukupnaCena + ", datumKupovine="
				+ datumKupovine + ", musterija=" + musterija + ", brojKupljenihKnjiga=" + brojKupljenihKnjiga + "]";
	}
	
	
	
	
}
	
	