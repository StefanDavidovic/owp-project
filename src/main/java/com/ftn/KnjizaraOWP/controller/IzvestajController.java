package com.ftn.KnjizaraOWP.controller;

import java.io.IOException;
import java.time.LocalDate;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.KnjizaraOWP.dao.KupovinaDao;
import com.ftn.KnjizaraOWP.model.Knjiga;
import com.ftn.KnjizaraOWP.model.Kupovina;
import com.ftn.KnjizaraOWP.service.KnjigaService;
import com.ftn.KnjizaraOWP.service.KupovinaService;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value="/Izvestaj")
public class IzvestajController {
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;

	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private KupovinaService kupovinaService;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@GetMapping("menjajLokalizacijuNaSrpski")
	public void index2(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na sr");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("sr"));
		
		response.sendRedirect(baseURL + "Izvestaj");
	}	
	
	@GetMapping("menjajLokalizacijuNaEngleski")
	public void index3(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na en");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("en"));
		
		response.sendRedirect(baseURL + "Izvestaj");
	}	

	
	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) Integer id,
			@RequestParam(required=false) Integer knjigaId,
			@RequestParam(required=false) Float ukupnaCena,
			@RequestParam(required=false) @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datumKupovineOd,
			@RequestParam(required=false) @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate datumKupovineDo,
			@RequestParam(required=false) String musterijaOznaka,
			@RequestParam(required=false) Integer brojKupljenihKnjiga,
			HttpSession session) throws IOException{
		
			List<Kupovina> kupovine = kupovinaService.findAll2();
			
			ModelAndView rezultat = new ModelAndView("izvestaj");
			rezultat.addObject("kupovine", kupovine);
			
			
			int cenaKonacna = 0;
			for(int  i=0; i < kupovine.size();i++ ) {
				cenaKonacna = (kupovine.get(i).getBrojKupljenihKnjiga() * kupovine.get(i).getUkupnaCena().intValue());
				System.out.println(cenaKonacna);
				rezultat.addObject("cenaKonacna", cenaKonacna);
			}
			
			
			

			
			
			return rezultat;
		
			}


}
