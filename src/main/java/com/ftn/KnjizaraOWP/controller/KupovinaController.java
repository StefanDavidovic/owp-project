package com.ftn.KnjizaraOWP.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.KnjizaraOWP.model.Kupovina;
import com.ftn.KnjizaraOWP.service.KupovinaService;


@Controller
@RequestMapping(value="/kupovine")
public class KupovinaController {
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	@Autowired
	private KupovinaService kupovinaService;

	@Autowired
	private LocaleResolver localeResolver;
	
	@GetMapping("menjajLokalizacijuNaSrpski")
	public void index2(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na sr");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("sr"));
		
		response.sendRedirect(baseURL + "kupovine");
	}	
	
	@GetMapping("menjajLokalizacijuNaEngleski")
	public void index3(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na en");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("en"));
		
		response.sendRedirect(baseURL + "kupovine");
	}	
	
	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam Integer id, HttpSession session, HttpServletRequest request,
				HttpServletResponse response) throws IOException {
					System.out.println("PROBA KUPOVINEEEE");
					Kupovina kupovina = kupovinaService.findOne(id);
					System.out.println(kupovina);
					
					ModelAndView rezultat = new ModelAndView("kupovina");
					
					rezultat.addObject("kupovina", kupovina);


					
					return rezultat;
				}


}
