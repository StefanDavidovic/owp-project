package com.ftn.KnjizaraOWP.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.KnjizaraOWP.model.Korisnik;
import com.ftn.KnjizaraOWP.model.LoyaltyKartica;
import com.ftn.KnjizaraOWP.service.KorisnikService;
import com.ftn.KnjizaraOWP.service.LoyaltyKarticaService;


@Controller
@RequestMapping(value="/loyaltyKartica")
public class LoyaltyKarticaController {
	
public static final String KARTICA_KEY = "loyaltyKartica";
	
	public static final String KORISNIK_KEY = "prijavljeniKorisnik";

	
	@Autowired
	private LoyaltyKarticaService loyaltyKarticaService;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		
	}
	

	
	
	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) Integer id,
			@RequestParam(required=false) String popust,
			@RequestParam(required=false) Integer brPoena,
			@RequestParam(required=false) Integer vlasnikOznaka,
			@RequestParam(required=false) boolean status,
			HttpSession session, HttpServletResponse response) throws IOException{
			
			Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
			List<LoyaltyKartica> loyaltyKartice = loyaltyKarticaService.findAll();
			System.out.println(loyaltyKarticaService.findAll2()+ "!!!!!!!!!!!!!!! KORISNICKO" );
			ModelAndView rezultat = new ModelAndView("loyaltyKartice");
			ModelAndView rezultat2 = new ModelAndView("zahtevLoyaltyKartice");
			rezultat.addObject("loyaltyKartice", loyaltyKartice);
			rezultat2.addObject("prijavljeniKorisnik", prijavljeniKorisnik);
			
			return rezultat;
			
	}


	
	@GetMapping(value="/Create")
	public ModelAndView create(HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik  = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik == null){
			response.sendRedirect(baseURL + "index");
			return null;
		}
		//ModelAndView rezultat = new ModelAndView("dodavanjeKnjige");
		
		return null;
	}
	
	
	@GetMapping(value="/generisiKarticu")
	public void generateCard(HttpSession session, HttpServletResponse response) throws IOException {
		System.out.println("LOYAAAALTYYY");
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		
		Korisnik korisnik = korisnikService.findOne(prijavljeniKorisnik.getKorisnickoIme());
		
		LoyaltyKartica kartica = new LoyaltyKartica(0, 0, korisnik, false);
		loyaltyKarticaService.save(kartica);
		
		response.sendRedirect(baseURL + "knjige");
	}
	
	
	@PostMapping(value="/Create")
	public void create(@RequestParam(required=false) Integer id,
			@RequestParam (required=false) Integer popust,
			@RequestParam (required=false) Integer brPoena,
			@RequestParam Integer vlasnikOznaka,
			//(defaultValue="Na cekanju")
			@RequestParam boolean status,
			HttpSession session, HttpServletResponse response) throws IOException{
		//aut
		//Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		/*if(prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL+ "index");
			return;
			}*/
		
		Korisnik prijavljeniKorisnik  = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if(prijavljeniKorisnik == null){
			response.sendRedirect(baseURL + "Knjige");
			return;
			}
		
		if(vlasnikOznaka != prijavljeniKorisnik.getId()) {
			response.sendRedirect(baseURL);
			return;
			}
			
		session.setAttribute(KorisnikController.KORISNIK_KEY, prijavljeniKorisnik);	
		
		
//		Korisnik vlasnik = korisnikService.find
//		if(vlasnik == null) {
//			response.sendRedirect(baseURL);
//			
//		}
//		
//		LoyaltyKartica loyaltyKartica = new LoyaltyKartica(popust, brPoena, vlasnik, status);
//		loyaltyKarticaService.save(loyaltyKartica);
//		
		response.sendRedirect(baseURL + "Knjige");
		//ovo

		}
	
	
	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam Integer id, HttpSession session, HttpServletRequest request,
				HttpServletResponse response) throws IOException {
					
					LoyaltyKartica kartica = loyaltyKarticaService.findOne(id);
					
					ModelAndView rezultat = new ModelAndView("loyaltyKartica");
					rezultat.addObject("kartica", kartica);
					
					return rezultat;
				}
			
	@GetMapping("menjajLokalizacijuNaSrpski")
	public void index2(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na sr");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("sr"));
		
		response.sendRedirect(baseURL+"LoyaltyKartice");
	}	
	
	@GetMapping("menjajLokalizacijuNaEngleski")
	public void index3(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na en");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("en"));
		
		response.sendRedirect(baseURL+"LoyaltyKartice");
	}	
	
	@PostMapping(value="/Edit")
	public void edit(@RequestParam(required=false) Integer id,
					@RequestParam(required=false) String popust,
					@RequestParam(required=false) Integer brPoena,
					@RequestParam(required=false) String vlasnikOznaka,
					@RequestParam(required=false) boolean status,
					HttpSession session, HttpServletResponse response) throws IOException{
		
					Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
					
					if(prijavljeniKorisnik == null) {
						response.sendRedirect(baseURL + "LoyaltyKartice");
						return;
					}
						
					LoyaltyKartica kartica = loyaltyKarticaService.findOne(id);
					if(kartica == null) {
						response.sendRedirect(baseURL + "LoyaltyKartice");
					}
						kartica.setStatus(status);
						
						loyaltyKarticaService.update(kartica);
						response.sendRedirect(baseURL+"LoyaltyKartice");
						
					}
	
	
	@PostMapping(value="/accept")
	public void accept(@RequestParam(required=false) int korisnikId,
					@RequestParam(required=false) Integer popust,
					@RequestParam(required=false) Integer brPoena,
					@RequestParam(required=false) Integer vlasnikOznaka,
					@RequestParam(required=false) boolean status,
					HttpSession session, HttpServletResponse response) throws IOException{
		
					Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
					
					if(prijavljeniKorisnik == null) {
						response.sendRedirect(baseURL + "loyaltyKartica");
						return;
					}
					
					System.out.println("Prijavljeni " + prijavljeniKorisnik);
					System.out.println(korisnikId + "ID ");
						
					LoyaltyKartica kartica = loyaltyKarticaService.findOne(korisnikId);
					if(kartica == null) {
						response.sendRedirect(baseURL + "loyaltyKartica");
					}
					System.out.println("KARTICAA" + kartica);
						kartica.setBrPoena(4);
						kartica.setPopust(20);
						kartica.setStatus(true);
						
						loyaltyKarticaService.update(kartica);
						response.sendRedirect(baseURL+"loyaltyKartica");
						
						
					}
	
	@PostMapping(value="/delete")
	public void delete(@RequestParam(required=false) Integer id,HttpSession session, HttpServletResponse response)throws IOException{
//		LoyaltyKartica kartica = loyaltyKarticaService.findOne(id);
//		if(kartica == null) {
//			response.sendRedirect(baseURL + "loyaltyKartica");
//		}
		System.out.println("OVO JE DELETEEEEE" + id);
		loyaltyKarticaService.delete(id);
		System.out.println("OVO JE DELETEEEEE ponovoo " + id);
		
		response.sendRedirect(baseURL+"loyaltyKartica");
		
	}
					


}
