package com.ftn.KnjizaraOWP.controller;



import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;

import com.ftn.KnjizaraOWP.model.Knjiga;
import com.ftn.KnjizaraOWP.model.Korisnik;
import com.ftn.KnjizaraOWP.model.Kupovina;
import com.ftn.KnjizaraOWP.model.LoyaltyKartica;
import com.ftn.KnjizaraOWP.service.KnjigaService;
import com.ftn.KnjizaraOWP.service.KorisnikService;
import com.ftn.KnjizaraOWP.service.KupovinaService;
import com.ftn.KnjizaraOWP.service.LoyaltyKarticaService;
import com.ftn.KnjizaraOWP.service.ShoppingCartService;

@Controller
public class ShoppingCartController {
	
	private static final Logger logger = LoggerFactory.getLogger(ShoppingCartController.class);
    private final ShoppingCartService shoppingCartService;
    private final KnjigaService productService;
    
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 
	
	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private KorisnikService korisnikService;

	@Autowired
	private KupovinaService kupovinaService;
	
	@Autowired
	private LoyaltyKarticaService loyaltyKarticaService;

	@Autowired
	private LocaleResolver localeResolver;
	
	@GetMapping("menjajLokalizacijuNaSrpski")
	public void index2(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na sr");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("sr"));
		
		response.sendRedirect(baseURL + "cart");
	}	
	
	@GetMapping("menjajLokalizacijuNaEngleski")
	public void index3(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na en");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("en"));
		
		response.sendRedirect(baseURL + "cart");
	}	
	
    @Autowired
    public ShoppingCartController(ShoppingCartService shoppingCartService, KnjigaService productService) {
        this.shoppingCartService = shoppingCartService;
        this.productService = productService;
    }

    @GetMapping("/cart")
    public String cart(Model model){
        model.addAttribute("products", shoppingCartService.productsInCart());
        model.addAttribute("totalPrice", shoppingCartService.totalPrice());

        return "cart";
    }

    @GetMapping("/cart/add/{id}")
    public String addProductToCart(@PathVariable("id") long id, @RequestParam("kolicina") int kolicina){
        Knjiga product = productService.findOne(id);
        if (product != null){
            shoppingCartService.addProduct(product, kolicina);
            logger.debug(String.format("Product with id: %s added to shopping cart.", id));
        }
        return "redirect:/knjige";
    }

    @GetMapping("/cart/remove/{id}")
    public String removeProductFromCart(@PathVariable("id") long id){
    	Knjiga product = productService.findOne(id);
    	System.out.println("ovo je u controlleru " + product.getNaziv());
        if (product != null){
            shoppingCartService.removeProduct(product);
            logger.debug(String.format("Product with id: %s removed from shopping cart.", id));
        }
        return "redirect:/cart";
    }

    @GetMapping("/cart/clear")
    public String clearProductsInCart(){
        shoppingCartService.clearProducts();

        return "redirect:/cart";
    }

    @GetMapping("/cart/checkout")
    public String cartCheckout(){
        shoppingCartService.cartCheckout();

        return "redirect:/cart";
    }
    
    @PostMapping("/cart/order")
    public void createOrder(@RequestParam Integer id,
			HttpSession session, HttpServletResponse response) throws IOException  {
    	
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL + "knjige");
			return;
		}

		Knjiga knjiga1 = productService.findOne(id);
		if(knjiga1 == null) {
			response.sendRedirect(baseURL + "KorisnickaKorpa");
			return;
		}
		
		Korisnik korisnik = korisnikService.findOne(prijavljeniKorisnik.getKorisnickoIme());
		
		BigDecimal cena = shoppingCartService.totalPrice();
		LocalDate datum = LocalDate.now();
		
		Date datumOd = knjiga1.getDatumOd();
		Date datumDo = knjiga1.getDatumDo();

		GregorianCalendar gregorianCalendar = (GregorianCalendar) Calendar.getInstance();
		gregorianCalendar.setTime(datumOd);
		ZonedDateTime zonedDateTime = gregorianCalendar.toZonedDateTime();
		zonedDateTime.toLocalDate();
		
		GregorianCalendar gregorianCalendar2 = (GregorianCalendar) Calendar.getInstance();
		gregorianCalendar2.setTime(datumDo);
		ZonedDateTime zonedDateTime2 = gregorianCalendar2.toZonedDateTime();
		zonedDateTime2.toLocalDate();
		

		Integer cena2;
		
		System.out.println(datum.isBefore(zonedDateTime2.toLocalDate()));
		
		if (datum.isAfter(zonedDateTime.toLocalDate()) && datum.isBefore(zonedDateTime2.toLocalDate())) {
			
			System.out.println("IDE SE PO DATUMUUU");
			
			Integer popust = knjiga1.getPopust();
			if (popust < 2) {
				popust = 0;
			}
			cena2 = ((cena.intValue() / 100) * (100-(popust)));
			BigDecimal bd3 = new BigDecimal(cena2);
			
			Kupovina kupovina = new Kupovina(knjiga1,bd3,datum,korisnik,shoppingCartService.numberEl());
			kupovinaService.save(kupovina);
			
			knjiga1.setKolicina(knjiga1.getKolicina()-shoppingCartService.numberEl());
			knjigaService.update(knjiga1);

		}else {
			LoyaltyKartica kartica = loyaltyKarticaService.findOne(Math.toIntExact(prijavljeniKorisnik.getId()));
			
			System.out.println("IDE SE PO KARTICIII");
			
			if(kartica != null) {
				System.out.println("STVARNO IDE OVUDA");
				System.out.println("OVO JE KARTICAA" + kartica);
				Integer brojBodova = (int)(cena.intValue()/1000);
				Integer stariBrojBodova = kartica.getBrPoena();
				System.out.println(stariBrojBodova + "STARI BR BODODVA");
				if(stariBrojBodova < 1) {
					stariBrojBodova = 1;
				}else {
					kartica.setBrPoena(brojBodova+stariBrojBodova);}
				if (kartica.getBrPoena() > 10) {
					kartica.setBrPoena(10);
				}
				System.out.println(kartica.getBrPoena() + "NOVI BR BODOVA");
				cena2 =((cena.intValue() / 100) * (100-(kartica.getBrPoena() * 5)));
				System.out.println(cena2 + " " + cena + "CEMA");
				BigDecimal bd3 = new BigDecimal(cena2);
				Kupovina kupovina = new Kupovina(knjiga1,bd3,datum,korisnik,shoppingCartService.numberEl());
				kupovinaService.save(kupovina);
				kartica.setBrPoena(0);
				kartica.setPopust(0);
				
				loyaltyKarticaService.update(kartica);
				
				knjiga1.setKolicina(knjiga1.getKolicina()-shoppingCartService.numberEl());
				knjigaService.update(knjiga1);
				
				System.out.println("IMA KARTICUUUUU" + kartica + " " + kupovina);

			}else {
				
				System.out.println("IDE SE KLASICNOOOO");
				
				Kupovina kupovina = new Kupovina(knjiga1,cena,datum,korisnik,shoppingCartService.numberEl());
				kupovinaService.save(kupovina);
				
				knjiga1.setKolicina(knjiga1.getKolicina()-shoppingCartService.numberEl());
				knjigaService.update(knjiga1);

			}
		}
		
		
		

		
//		LoyaltyKartica kartica = loyaltyKarticaService.findOne(Math.toIntExact(prijavljeniKorisnik.getId()));
//		System.out.println("OVO JE KARTICAA" + kartica);
//		if(kartica != null) {
//			System.out.println("OVO JE KARTICAA" + kartica);
//			Integer brojBodova = (int)(cena.intValue()/1000);
//			Integer stariBrojBodova = kartica.getBrPoena();
//			kartica.setBrPoena(brojBodova+stariBrojBodova);
//			if (kartica.getBrPoena() > 10) {
//				kartica.setBrPoena(10);
//			}
//			cena2 = cena2 + ((cena.intValue() / 100) * 100-(brojBodova * 5));
//			BigDecimal bd3 = new BigDecimal(cena2);
//			Kupovina kupovina = new Kupovina(knjiga1,bd3,datum,korisnik,shoppingCartService.numberEl());
//			kupovinaService.save(kupovina);
//			kartica.setBrPoena(0);
//			kartica.setPopust(0);
//			
//			loyaltyKarticaService.update(kartica);
//			
//			System.out.println("IMA KARTICUUUUU" + kartica + " " + kupovina);
//
//		}else {
//			Kupovina kupovina = new Kupovina(knjiga1,cena,datum,korisnik,shoppingCartService.numberEl());
//			kupovinaService.save(kupovina);
//			System.out.println("NEMA KARTICUUUUU" + kartica + " " + kupovina);
//		}
		
		
		
		shoppingCartService.cartCheckout();
		
		response.sendRedirect("http://localhost:8086/knjizara/cart");
    }

}
