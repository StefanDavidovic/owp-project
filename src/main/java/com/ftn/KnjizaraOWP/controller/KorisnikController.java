 package com.ftn.KnjizaraOWP.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.KnjizaraOWP.model.Korisnik;
import com.ftn.KnjizaraOWP.model.Kupovina;
import com.ftn.KnjizaraOWP.service.KorisnikService;
import com.ftn.KnjizaraOWP.service.KupovinaService;
import com.ftn.KnjizaraOWP.service.ShoppingCartService;
import com.ftn.KnjizaraOWP.controller.KorisnikController;


@Controller
@RequestMapping(value="/korisnici")
public class KorisnikController {
	
	public static final String KORISNIK_KEY = "prijavljeniKorisnik";
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private KupovinaService kupovinaService;
	
	@Autowired
	private ShoppingCartService shoppingCarService;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL; 

	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";			
	}
	
	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) Long id,
			@RequestParam(required=false) String korisnickoIme,
			@RequestParam(required=false) String lozinka,
			@RequestParam(required=false) String eMail,
			@RequestParam(required=false) String ime,
			@RequestParam(required=false) String prezime,
			@RequestParam(required=false) String pol,
			@RequestParam(required=false) Boolean administrator,
			@RequestParam(required=false) Date datum,
			@RequestParam(required=false) Date datumRegistracije,
			@RequestParam(required=false) String adresa,
			@RequestParam(required=false) String broj,
			@RequestParam(required=false) Boolean blokiran,
			HttpSession session, HttpServletResponse response) throws IOException {		
		// autentikacija, autorzacija
//		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
//		System.out.println(prijavljeniKorisnik);
//		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
//			response.sendRedirect(baseURL);
//			return null;
//		}
		
//		System.out.println(korisnickoIme);
//
//		//ako je input tipa text i ništa se ne unese 
//		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
//		if(korisnickoIme!=null && korisnickoIme.trim().equals(""))
//			korisnickoIme=null;
//		
//		if(eMail!=null && eMail.trim().equals(""))
//			eMail=null;
//		
//		if(pol!=null && pol.trim().equals(""))
//			pol=null;
		
		// čitanje
		List<Korisnik> korisnici = korisnikService.find(id, korisnickoIme,lozinka, eMail,ime, prezime, pol, administrator,datum, datumRegistracije,adresa, broj,blokiran);
		System.out.println("TESTIRANJEEE");
		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", korisnici);

		return rezultat;
	}

	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam String korisnickoIme, 
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

		
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL + "korisnici");
			return null;
		}

		
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		if (korisnik == null) {
			response.sendRedirect(baseURL + "korisnici");
			return null;
		}
		
		

		List<Kupovina> kupovine = kupovinaService.findd(korisnik.getId());
		System.out.println(kupovine);
//		for (int i = 0; i < kupovine.size(); i++) {
//			System.out.println(kupovine.get(i).getMusterija());
//			if(kupovine.get(i).getMusterija() == korisnik)
//		}
		
		
		
		
		ModelAndView rezultat = new ModelAndView("korisnik");
		rezultat.addObject("korisnik", korisnik);
		rezultat.addObject("kupovine",kupovine);
		rezultat.addObject("products",shoppingCarService.productsInCart());

		return rezultat;
	}

	@GetMapping(value="/Create")
	public String create(HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		// samo administrator može da kreira korisnike
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Filmovi");
			return "filmovi";
		}

		return "dodavanjeKorisnika";
	}
	
	@PostMapping(value="/Create")
	public void create(@RequestParam String korisnickoIme, @RequestParam String lozinka, @RequestParam@DateTimeFormat LocalDate datum,
			@RequestParam String eMail,@RequestParam String ime,@RequestParam String prezime, @RequestParam String pol, @RequestParam(required=false) String administrator,@RequestParam String adresa,@RequestParam String broj,
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		// samo administrator može da kreira korisnike
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Korisnici");
			return;
		}

		// validacija
		Korisnik postojeciKorisnik = korisnikService.findOne(korisnickoIme);
		if (postojeciKorisnik != null) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (korisnickoIme.equals("") || lozinka.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (eMail.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		if (!pol.equals("muški") && !pol.equals("ženski")) {
			response.sendRedirect(baseURL + "Korisnici/Create");
			return;
		}
		
		LocalDate datum2 = LocalDate.now();
		Date datumRegistracije = java.sql.Date.valueOf(datum2);
		// kreiranje
		Date datum1 = java.sql.Date.valueOf(datum);
		
		Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, eMail,ime, prezime, pol, administrator != null, datum1, datumRegistracije,adresa, broj, false);
		korisnikService.save(korisnik);

		response.sendRedirect(baseURL + "Korisnici");
	}

	@PostMapping(value="/Edit")
	public void edit(@RequestParam String korisnickoIme, 
			@RequestParam String eMail,@RequestParam String ime,@RequestParam String prezime, @RequestParam String pol, @RequestParam(required=false) String administrator, @RequestParam(required=false) boolean blokiran, 
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		// samo administrator može da menja druge korisnike; svaki korisnik može da menja sebe
		if (prijavljeniKorisnik == null || (!prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.getKorisnickoIme().equals(korisnickoIme))) {
			response.sendRedirect(baseURL + "Korisnici");
			return;
		}

		// validacija
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		if (korisnik == null) {
			response.sendRedirect(baseURL + "Korisnici");
			return;
		}
		if (eMail.equals("")) {
			response.sendRedirect(baseURL + "Korisnici/Edit?korisnicoIme=" + korisnickoIme);
			return;
		}
		if (!pol.equals("muški") && !pol.equals("ženski")) {
			response.sendRedirect(baseURL + "Korisnici/Edit?korisnicoIme=" + korisnickoIme);
			return;
		}

		// izmena
//		if (!lozinka.equals("")) {
//			korisnik.setLozinka(lozinka);
//		}
		korisnik.seteMail(eMail);
		korisnik.setPol(pol);
		korisnik.setIme(ime);
		korisnik.setPrezime(prezime);
		// privilegije može menjati samo administrator i to drugim korisnicima
		if (prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.equals(korisnik)) {
			korisnik.setAdministrator(administrator != null);
		}
		
		korisnik.setBlokiran(blokiran);
		
		korisnikService.update(korisnik);

		// sigurnost
		if (!prijavljeniKorisnik.equals(korisnik)) {
			// TODO odjaviti korisnika
		}

		if (prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "korisnici");
		} else {
			response.sendRedirect(baseURL);
		}
	}

	@PostMapping(value="/Delete")
	public void delete(@RequestParam String korisnickoIme, 
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		// samo administrator može da briše korisnike, ali ne i sebe
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator() || prijavljeniKorisnik.getKorisnickoIme().equals(korisnickoIme)) {
			response.sendRedirect(baseURL + "Korisnici");
			return;
		}

		// brisanje
		korisnikService.delete(korisnickoIme);

		// sigurnost
		// TODO odjaviti korisnika
		
		response.sendRedirect(baseURL + "Korisnici");
	}
	
	@PostMapping(value="/Register")
	public ModelAndView register(@RequestParam String korisnickoIme, @RequestParam String lozinka, @RequestParam String ponovljenaLozinka,
			@RequestParam String eMail,@RequestParam String ime,@RequestParam String prezime, @RequestParam String pol,
			@RequestParam(required=false)@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate datum,@RequestParam String adresa,@RequestParam String broj,
			HttpSession session, HttpServletResponse response) throws IOException {
		try {
			// validacija
			Korisnik postojeciKorisnik = korisnikService.findOne(korisnickoIme);
			if (postojeciKorisnik != null) {
				throw new Exception("Korisničko ime već postoji!");
			}
			if (korisnickoIme.equals("") || lozinka.equals("")) {
				throw new Exception("Korisničko ime i lozinka ne smeju biti prazni!");
			}
			if (!lozinka.equals(ponovljenaLozinka)) {
				throw new Exception("Lozinke se ne podudaraju!");
			}
			if (eMail.equals("")) {
				throw new Exception("E-mail ne sme biti prazan!");
			}
			if (!pol.equals("muški") && !pol.equals("ženski")) {
				throw new Exception("Morate odabrati pol!");
			}

			
			LocalDate datum2 = LocalDate.now();
			Date datumRegistracije = java.sql.Date.valueOf(datum2);

			// registracija
			System.out.println("PRVA REGeeee");
			Date datum1 = java.sql.Date.valueOf(datum);
			Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, eMail,ime, prezime, pol, false, datum1,datumRegistracije, adresa, broj, false);
			System.out.println(korisnik);
			korisnikService.save(korisnik);
			System.out.println("REGISTRACIJAA222");

			response.sendRedirect(baseURL);
			return null;
		} catch (Exception ex) {
			// ispis greške
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna registracija!";
			}

			// prosleđivanje
			ModelAndView rezultat = new ModelAndView("registracija");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}
	
	@PostMapping(value="/Login")
	public ModelAndView postLogin(@RequestParam String korisnickoIme, @RequestParam String lozinka, 
			HttpSession session, HttpServletResponse response) throws IOException {
//		System.out.println("TRAZIIII");
//		Korisnik korisnik = korisnikService.findOne(korisnickoIme, lozinka);
//		System.out.println("NASAO" + korisnik);
////		if (korisnik == null) {
////			throw new Exception("Neispravno korisničko ime ili lozinka!");
////		}			
//
//		// prijava
//		session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);
//		
//		response.sendRedirect(baseURL);
		
//		return null;
		try {
			// validacija
			System.out.println("EOGAAAAAA");
			Korisnik korisnik = korisnikService.findOne(korisnickoIme, lozinka);
			System.out.println(korisnik+ "NASAOOOO");
			if (korisnik == null || korisnik.isBlokiran() == true) {
				throw new Exception("Neispravno korisničko ime ili lozinka!");
			}			

			// prijava
			session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);
			System.out.println("EOGAAAAAA");
			response.sendRedirect(baseURL);
			return null;
		} catch (Exception ex) {
			// ispis greške
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna prijava!";
			}
//			response.sendRedirect(baseURL+"/korisnici/Login");
			// prosleđivanje
			ModelAndView rezultat = new ModelAndView("prijava");
			rezultat.addObject("poruka", poruka);
			

			return rezultat;
		}
	}

	@GetMapping(value="/Logout")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {
		// odjava	
		session.invalidate();
		
		response.sendRedirect(baseURL);
	}
	
	@GetMapping("menjajLokalizacijuNaSrpski")
	public void index2(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na sr");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("sr"));
		
		response.sendRedirect(baseURL + "korisnici");
	}	
	
	@GetMapping("menjajLokalizacijuNaEngleski")
	public void index3(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na en");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("en"));
		
		response.sendRedirect(baseURL + "korisnici");
	}	
	

}
