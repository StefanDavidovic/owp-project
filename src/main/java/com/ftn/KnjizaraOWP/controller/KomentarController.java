package com.ftn.KnjizaraOWP.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.KnjizaraOWP.model.Knjiga;
import com.ftn.KnjizaraOWP.model.Komentar;
import com.ftn.KnjizaraOWP.model.Korisnik;
import com.ftn.KnjizaraOWP.model.Kupovina;
import com.ftn.KnjizaraOWP.model.LoyaltyKartica;
import com.ftn.KnjizaraOWP.model.StatusKomentara;
import com.ftn.KnjizaraOWP.service.KnjigaService;
import com.ftn.KnjizaraOWP.service.KomentarService;
import com.ftn.KnjizaraOWP.service.KupovinaService;

@Controller
@RequestMapping(value="/Komentari")
public class KomentarController {
	
	
	@Autowired
	private KomentarService komentarService;
	@Autowired
	private KupovinaService kupovinaService;
	
	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;

	
	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@GetMapping("menjajLokalizacijuNaSrpski")
	public void index2(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na sr");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("sr"));
		
		response.sendRedirect(baseURL + "Komentari");
	}	
	
	@GetMapping("menjajLokalizacijuNaEngleski")
	public void index3(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na en");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("en"));
		
		response.sendRedirect(baseURL + "Komentari");
	}	
	
	
	@GetMapping
	public ModelAndView index(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KorisnikController.KORISNIK_KEY);
		if(korisnik==null) {
			response.sendRedirect(baseURL+"login.html");
		}
		// čitanje
		List<Komentar> komentari = komentarService.findAll();
		System.out.println("OVO SU KOMENTARI" + komentari);
		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("komentari");
//		boolean kupljena = jelKupio(korisnik);
		rezultat.addObject("komentari", komentari);
//		rezultat.addObject("kupljena", kupljena);
		return rezultat;
	}
	
	
	@GetMapping("/reject")
	public void OdbijanjeKomentara(Long id, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		Komentar komentar = komentarService.findOne(id);
		
		komentar.setStatus(StatusKomentara.nijeOdobren);
		komentarService.update(komentar);
		response.sendRedirect(baseURL + "/Komentari");
		
		

	}
	
	@GetMapping("/getKomentare/{id}")
	public ModelAndView index2(@PathVariable("id") long id,
			HttpSession session) throws IOException {
		
		
		System.out.println("POGODJEEEN");
//		List<Komentar> komentari = komentarService.findAll();
		
//		System.out.println("POGODJEEEN" + komentari);
		Knjiga knjiga = knjigaService.findOne(id);

		ModelAndView rezultat = new ModelAndView("knjiga");
//		boolean kupljena = jelKupio(korisnik);
//		rezultat.addObject("komentari", komentari);
		rezultat.addObject("knjiga", knjiga);
//		rezultat.addObject("kupljena", kupljena);
		return rezultat;
	}
	
	
	@RequestMapping(
			  value = "/getComments/{id}", 
			  method = RequestMethod.GET, 
			  produces = "application/json"
			)
			@ResponseBody
			public Map<String, Object> getComments(@PathVariable("id") long id) {
			    HashMap<String, Object> map = new HashMap<String, Object>();
			    
			    
			    List<Komentar> komentari = komentarService.findAll();
			    Knjiga knjige = knjigaService.findOne(id);
			    
			    map.put("komentari", komentari);
			    map.put("knjiga", knjige);
			    return map;
			}
	
	
	@GetMapping("/accept")
	public void OdobravanjeKomentara(Long id, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		Komentar komentar = komentarService.findOne(id);
		
		komentar.setStatus(StatusKomentara.odobren);
		
		Knjiga knjiga = knjigaService.findOne(komentar.getKnjiga().getId());
		
		double ocena = knjiga.getProsecnaOcena();
		
		if(ocena != 1) {
			knjiga.setProsecnaOcena((ocena+komentar.getOcena())/2);
			knjigaService.update(knjiga);
		}if(ocena == 1) {
			knjiga.setProsecnaOcena(komentar.getOcena());
			knjigaService.update(knjiga);
		}
		
		
		
		komentarService.update(komentar);
		response.sendRedirect(baseURL + "/Komentari");

	}
	
	
	@PostMapping("/Komentarisi")
	public ModelAndView dodajUKorpu(
			
			@RequestParam String korisnickoIme,
			@RequestParam String opis,
			@RequestParam Long knjigaId,
			@RequestParam Integer ocena,HttpSession session,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		try {
						
					Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KorisnikController.KORISNIK_KEY);
					if(korisnik==null || korisnik.isAdministrator()==true) {
						response.sendRedirect(baseURL+"prijava.html");
					}
					LocalDate datum1 = LocalDate.now();
					ZoneId systemTimeZone = ZoneId.systemDefault();
					ZonedDateTime zonedDateTime = datum1.atStartOfDay(systemTimeZone);
					
					Date datum =  Date.from(zonedDateTime.toInstant());
			
					
					Knjiga knjiga = knjigaService.findOne(knjigaId);
					System.out.println("OVO JE KNJIGA IZ KOMENTARA");
					if(knjiga==null) {
						response.sendRedirect(baseURL);
					}
					
					
					
					
//					if (jelKupio(korisnik) == false) {
//						System.out.println("PRVA GRESKA U KOMENTARU");
//						throw new Exception("Ne mozete komentarisati knjigu koju niste kupili");
//					}
//					if (jelKomantarisao(korisnik,knjiga) == false) {
//						System.out.println("Druga GRESKA U KOMENTARU");
//						throw new Exception("Ne mozete 2 puta komentarisati istu knjigu!");
//					}
//					
//					if (opis.equals("")) {
//						System.out.println("Treca GRESKA U KOMENTARU");
//						throw new Exception("Morate uneti opis");
//					}
//					
//					
//					if (ocena == null) {
//						System.out.println("Cetvrta GRESKA U KOMENTARU");
//						throw new Exception("Morate uneti ocenu");
//					}
					
					
					
					Komentar komentar = new Komentar(1L, opis, ocena, datum, korisnik, knjiga, StatusKomentara.naCekanju);
					
					komentarService.save(komentar);
					response.sendRedirect(baseURL);
					return null;
					
					
					
					
		}catch (Exception ex) {
			// ispis greške
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešno dodavanje komentara!";
			}
			
			Knjiga knjiga = knjigaService.findOne(knjigaId);


			// prosleđivanje
			ModelAndView rezultat = new ModelAndView("knjiga");
			// čitanje
			rezultat.addObject("knjiga", knjiga);

			rezultat.addObject("poruka", poruka);


			return rezultat;
		}
	}
	
	
//	public boolean jelKupio(Korisnik korisnik) {
//		List<Kupovina> kupovine = kupovinaService.findAll();
//		for (Kupovina kupovina : kupovine) {
//			if(kupovina.getMusterija().getKorisnickoIme().equals(korisnik.getKorisnickoIme())) {
//				return true;
//			}
//			
//		}
//		return false;
//	}
//	
//	
//	
//	public boolean jelKomantarisao(Korisnik korisnik,Knjiga knjiga) {
//		List<Komentar> komentari = komentarService.findAll();
//		for (Komentar komentar : komentari) {
//			if(komentar.getKorisnik().getKorisnickoIme().equals(korisnik.getKorisnickoIme()) && knjiga.getId() == komentar.getKnjiga().getId()) {
//					System.out.println("KOMENTARISANA JE VEC");
//					return false;
//			}
//			
//		}
//		System.out.println("NIJE");
//
//		return true;
//	}

}
