package com.ftn.KnjizaraOWP.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.KnjizaraOWP.dao.KnjigaDao;
import com.ftn.KnjizaraOWP.model.EPismo;
import com.ftn.KnjizaraOWP.model.EPovez;
import com.ftn.KnjizaraOWP.model.Knjiga;
import com.ftn.KnjizaraOWP.service.KnjigaService;

import ImageConf.FileUploadUtil;

import com.ftn.KnjizaraOWP.model.Korisnik;


@Controller
@RequestMapping(value="/knjige")
public class KnjigaController {
	
	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private ServletContext servletContext;
	private String baseURL; 
	
	@Autowired
	private LocaleResolver localeResolver;

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";	
	}
	

	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) Long id,
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) String isbn,
			@RequestParam(required=false) String izdavackaKuca,
			@RequestParam(required=false) String autor,
			@RequestParam(required=false) String opis, 
			@RequestParam(required=false) String jezik, 
			@RequestParam(required=false) Double cenaOd, 
			@RequestParam(required=false) Double cenaDo,
			@RequestParam(required=false) Double prosecnaOcena,
			@RequestParam(required=false) Integer brStrana,
			@RequestParam(required=false) Integer kolicina,
			@RequestParam(required=false) String povez,
			@RequestParam(required=false) String pismo,
			@RequestParam(required=false) String slika,
			@RequestParam(required=false) Date datumOd,
			@RequestParam(required=false) Date datumDo,
			@RequestParam(required=false) Integer popust,
			HttpSession session) throws IOException {
		
		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
		if(naziv!=null && naziv.trim().equals(""))
			naziv=null;
		if(isbn!=null && isbn.trim().equals(""))
			isbn=null;
		if(izdavackaKuca!=null && izdavackaKuca.trim().equals(""))
			izdavackaKuca=null;
		if(autor!=null && autor.trim().equals(""))
			autor=null;
		if(opis!=null && opis.trim().equals(""))
			opis=null;
		if(jezik!=null && jezik.trim().equals(""))
			jezik=null;
		// čitanje

//		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		List<Knjiga> knjige = knjigaService.find(id ,naziv, isbn, izdavackaKuca, autor,opis, jezik, cenaOd, cenaDo, prosecnaOcena, brStrana,kolicina, povez, pismo, slika, datumOd, datumDo, popust);
		List<Knjiga> sortirane = knjigaService.sortirajPoNazivu();
		System.out.println(sortirane);
//		List<Knjiga> knjige = knjigaService.findAll();
		// prosleđivanje
		System.out.println("UUUUUUUUUUUUUUUU");
		ModelAndView rezultat = new ModelAndView("knjige");
		rezultat.addObject("knjige", knjige);
		rezultat.addObject("sortirane", sortirane);
		return rezultat;
	}
	@GetMapping("menjajLokalizacijuNaSrpski")
	public void index2(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na sr");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("sr"));
		
		response.sendRedirect(baseURL + "knjige");
	}	
	
	@GetMapping("menjajLokalizacijuNaEngleski")
	public void index3(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na en");
		localeResolver.setLocale(request, response, Locale.forLanguageTag("en"));
		
		response.sendRedirect(baseURL + "knjige");
	}
	@RequestMapping(value="/sortirane")
	public ModelAndView index2(
			@RequestParam(required=false) Long id,
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) String isbn,
			@RequestParam(required=false) String izdavackaKuca,
			@RequestParam(required=false) String autor,
			@RequestParam(required=false) String opis, 
			@RequestParam(required=false) String jezik, 
			@RequestParam(required=false) Double cenaOd, 
			@RequestParam(required=false) Double cenaDo,
			@RequestParam(required=false) Double prosecnaOcena,
			@RequestParam(required=false) Integer brStrana,
			@RequestParam(required=false) Integer kolicina,
			@RequestParam(required=false) String povez,
			@RequestParam(required=false) String pismo,
			HttpSession session) throws IOException {
		
		if(naziv!=null && naziv.trim().equals(""))
			naziv=null;
		if(isbn!=null && isbn.trim().equals(""))
			isbn=null;
		if(izdavackaKuca!=null && izdavackaKuca.trim().equals(""))
			izdavackaKuca=null;
		if(autor!=null && autor.trim().equals(""))
			autor=null;
		if(opis!=null && opis.trim().equals(""))
			opis=null;
		if(jezik!=null && jezik.trim().equals(""))
			jezik=null;
		// čitanje

		
		List<Knjiga> knjige = knjigaService.sortirajPoNazivu();
//		List<Knjiga> knjige = knjigaService.findAll();
		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("knjige");
		rezultat.addObject("knjige", knjige);
	
		return rezultat;
	}
	
	
	@GetMapping(value="/details")
	public ModelAndView Details(@RequestParam Long id, 
			HttpSession session, HttpServletResponse response) throws IOException {
		// čitanje

		Knjiga knjiga = knjigaService.findOne(id);
		System.out.println(knjiga);
		if (knjiga == null) {
			response.sendRedirect(baseURL + "knjige");
			return null;
		}

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("knjiga");
		rezultat.addObject("knjiga", knjiga);


		return rezultat;
	}
	
	
	@PostMapping(value="/Create",  consumes = { "multipart/form-data" })
	public void Create(
			@RequestParam(required=false) Long id1,
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) String isbn,
			@RequestParam(required=false) String izdavackaKuca,
			@RequestParam(required=false) String autor,
			@RequestParam(required=false) String opis, 
			@RequestParam(required=false) String jezik, 
			@RequestParam(required=false) BigDecimal cena, 
//			@RequestParam(required=false) Double prosecnaOcena,
			@RequestParam(required=false) Integer brStrana,
			@RequestParam(required=false) Integer kolicina,
			@RequestParam(required=false) String povez,
			@RequestParam(required=false) String pismo,
			@RequestPart("slika") MultipartFile multipartFile ,

			HttpSession session, HttpServletResponse response) throws IOException {
		
		// autentikacija, autorizacija
		
//		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
//		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
//			response.sendRedirect(baseURL + "knjige");
//			return;
//		}
		

		
		System.out.println(cena);
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        
        Date datumOd = parseDate("9000-01-01");
        Date datumDo = parseDate("9999-01-01");
        
        Double prosecnaOcena = 1.0;

		// kreiranje
		System.out.println(knjigaService.findAll());
		System.out.println("EO GA");
		Long id = 1343245323523523L;
		Knjiga knjiga = new Knjiga(id, naziv, isbn ,izdavackaKuca, autor, opis, cena, brStrana,kolicina, povez, pismo,jezik, prosecnaOcena, fileName, datumOd, datumDo, 1);
//		knjiga.setSlika(fileName);
		String uploadDir = "knjiga-photos";
		FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
		System.out.println("GAGAGA");
		knjigaService.save(knjiga);
		
		response.sendRedirect(baseURL + "knjige");
	
	}
	
	 public static Date parseDate(String date) {
	     try {
	         return new SimpleDateFormat("yyyy-MM-dd").parse(date);
	     } catch (ParseException e) {
	         return null;
	     }
	  }
	
	
	@PostMapping(value="/Edit")
	public void Edit(
			@RequestParam(required=false) Long id, 
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) String isbn,
			@RequestParam(required=false) String izdavackaKuca,
			@RequestParam(required=false) String autor,
			@RequestParam(required=false) String opis, 
			@RequestParam(required=false) String jezik, 
			@RequestParam(required=false) BigDecimal cena, 
			@RequestParam(required=false) Double prosecnaOcena,
			@RequestParam(required=false) Integer brStrana,
			@RequestParam(required=false) Integer kolicina,
			@RequestParam(required=false) String povez,
			@RequestParam(required=false) String pismo,
			@RequestPart("slika") MultipartFile multipartFile ,
			@RequestParam(required=false) String datumOd,
			@RequestParam(required=false) String datumDo,
			@RequestParam(required=false) Integer popust,
			HttpSession session, HttpServletResponse response) throws IOException, ParseException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "knjige");
			return;
		}
		System.out.println("BUUUUUUU");
		// validacija
		Knjiga knjiga = knjigaService.findOne(id);
		System.out.println(knjiga);
		if (knjiga == null) {
			response.sendRedirect(baseURL + "knjige");
			return;
		}
		
		String slika = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		
		Date datum1 = new SimpleDateFormat("yyyy-MM-dd").parse(datumOd);
		Date datum2 = new SimpleDateFormat("yyyy-MM-dd").parse(datumDo);
		
		System.out.println(datum1);
		// izmena
		knjiga.setNaziv(naziv);
		knjiga.setIsbn(isbn);
		knjiga.setIzdavackaKuca(izdavackaKuca);
		knjiga.setAutor(autor);
		knjiga.setOpis(opis);
		knjiga.setJezik(jezik);
		knjiga.setCena(cena);
		knjiga.setProsecnaOcena(prosecnaOcena);
		knjiga.setBrStrana(brStrana);
		knjiga.setKolicina(kolicina);
		knjiga.setPovez(povez);
		knjiga.setPismo(pismo);
		knjiga.setSlika(slika);
		knjiga.setDatumOd(datum1);
		knjiga.setDatumDo(datum2);
		knjiga.setPopust(popust);
		System.out.println("JEEEEEEEEEE");
		knjigaService.update(knjiga);
		String uploadDir = "knjiga-photos";
		FileUploadUtil.saveFile(uploadDir, slika, multipartFile);
		System.out.println("NAAAJS");
	
		response.sendRedirect(baseURL + "knjige");
	}
	
	
	@PostMapping(value="/Delete")
	public void Delete(@RequestParam Long id, 
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "knjige");
			return;
		}

		// brisanje
		knjigaService.delete(id);

		response.sendRedirect(baseURL + "knjige");
	}
	
	@GetMapping(value="/Search")
	@ResponseBody
	public Map<String, Object> search(
			@RequestParam(required=false) Long id,
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) String isbn,
			@RequestParam(required=false) String izdavackaKuca,
			@RequestParam(required=false) String autor,
			@RequestParam(required=false) String opis, 
			@RequestParam(required=false) String jezik, 
			@RequestParam(required=false) Double cenaOd, 
			@RequestParam(required=false) Double cenaDo, 
			@RequestParam(required=false) Double prosecnaOcena,
			@RequestParam(required=false) Integer brStrana,
			@RequestParam(required=false) Integer kolicina,
			@RequestParam(required=false) String povez,
			@RequestParam(required=false) String pismo,
			@RequestParam(required=false) Integer popust,
			@RequestPart("slika") MultipartFile multipartFile ,HttpSession session)  throws IOException {
		System.out.println("Pogodio pretragu");
		
		//ako je input tipa text i ništa se ne unese 
		//a parametar metode Sting onda će vrednost parametra handeler metode biti "" što nije null
		if(naziv!=null && naziv.trim().equals(""))
			naziv=null;
		if(isbn!=null && isbn.trim().equals(""))
			isbn=null;
		if(izdavackaKuca!=null && izdavackaKuca.trim().equals(""))
			izdavackaKuca=null;
		if(autor!=null && autor.trim().equals(""))
			autor=null;
		if(opis!=null && opis.trim().equals(""))
			opis=null;
		if(jezik!=null && jezik.trim().equals(""))
			jezik=null;
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		
        Date datumOd = parseDate("9000-01-01");
        Date datumDo = parseDate("9999-01-01");
		// čitanje
		List<Knjiga> knjige = knjigaService.find(id, naziv, isbn, izdavackaKuca, autor,opis, jezik, cenaOd, cenaDo, prosecnaOcena, brStrana,kolicina, povez, pismo, fileName, datumOd, datumDo, popust);

		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("knjige", knjige);
		return odgovor;
	}
	
    @GetMapping(value="/getImage/{slika}")
    public ResponseEntity<ByteArrayResource> getImage(@PathVariable("slika") String image_src){
    	if(!image_src.equals("")||image_src != null ) {
    		try {
				Path filename = Paths.get("knjiga-photos", image_src);
				byte[] buffer = Files.readAllBytes(filename);
				ByteArrayResource byteArrayResource = new ByteArrayResource(buffer);
				return ResponseEntity.ok().contentLength(buffer.length).contentType(MediaType.parseMediaType("image/png")).body(byteArrayResource);
			} catch (Exception e) {
				// TODO: handle exception
			}
    	}
    	return ResponseEntity.badRequest().build();
    }
	
	

}
