SELECT * FROM knjizara.korisnici;UPDATE `knjizara`.`knjige`
SET
`id` = <{id: }>,
`naziv` = <{naziv: }>,
`isbn` = <{isbn: }>,
`izdavackaKuca` = <{izdavackaKuca: }>,
`autor` = <{autor: }>,
`opis` = <{opis: }>,
`cena` = <{cena: }>,
`brStrana` = <{brStrana: }>,
`povez` = <{povez: }>,
`pismo` = <{pismo: }>,
`jezikPisanja` = <{jezikPisanja: }>,
`prosecnaOcena` = <{prosecnaOcena: }>
WHERE `id` = <{expr}>;

UPDATE `knjizara`.`korisnici`
SET
`id` = <{id: }>,
`korisnickoIme` = <{korisnickoIme: }>,
`lozinka` = <{lozinka: }>,
`eMail` = <{eMail: }>,
`pol` = <{pol: }>,
`administrator` = <{administrator: 0}>,
`datumRodjenja` = <{datumRodjenja: }>,
`adresa` = <{adresa: }>,
`brTelefona` = <{brTelefona: }>
WHERE `id` = <{expr}>;

UPDATE `knjizara`.`zanrovi`
SET
`id` = <{id: }>,
`naziv` = <{naziv: }>,
`opis` = <{opis: }>
WHERE `id` = <{expr}>;
CREATE TABLE `knjige` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` varchar(256) NOT NULL,
  `isbn` varchar(13) DEFAULT NULL,
  `izdavackaKuca` varchar(75) DEFAULT NULL,
  `autor` varchar(75) DEFAULT NULL,
  `opis` varchar(256) DEFAULT NULL,
  `cena` decimal(8,2) DEFAULT NULL,
  `brStrana` int DEFAULT NULL,
  `povez` varchar(34) DEFAULT NULL,
  `pismo` varchar(34) DEFAULT NULL,
  `jezikPisanja` varchar(20) DEFAULT NULL,
  `prosecnaOcena` decimal(2,1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `isbn` (`isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

CREATE TABLE `korisnici` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `korisnickoIme` varchar(20) DEFAULT NULL,
  `lozinka` varchar(20) NOT NULL,
  `eMail` varchar(20) NOT NULL,
  `pol` varchar(20) DEFAULT NULL,
  `administrator` tinyint(1) DEFAULT '0',
  `datumRodjenja` date DEFAULT NULL,
  `adresa` varchar(75) DEFAULT NULL,
  `brTelefona` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

CREATE TABLE `zanrovi` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` varchar(75) NOT NULL,
  `opis` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
