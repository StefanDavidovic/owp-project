CREATE TABLE `korisnici` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `korisnickoIme` varchar(20) DEFAULT NULL,
  `lozinka` varchar(20) NOT NULL,
  `eMail` varchar(20) NOT NULL,
  `pol` enum('muški','ženski') DEFAULT 'muški',
  `administrator` tinyint(1) DEFAULT '0',
  `datumRodjenja` datetime DEFAULT NULL,
  `adresa` varchar(75) DEFAULT NULL,
  `brTelefona` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `knjige` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` varchar(256) NOT NULL,
  `isbn` varchar(13) DEFAULT NULL,
  `izdavackaKuca` varchar(75) DEFAULT NULL,
  `autor` varchar(75) DEFAULT NULL,
  `opis` varchar(256) DEFAULT NULL,
  `cena` decimal(8,2) DEFAULT NULL,
  `brStrana` int DEFAULT NULL,
  `povez` enum('tvrdi','meki') DEFAULT 'meki',
  `pismo` enum('latinica','cirilica') DEFAULT 'latinica',
  `jezikPisanja` varchar(20) DEFAULT NULL,
  `prosecnaOcena` decimal(2,1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `isbn` (`isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `zanrovi` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` varchar(75) NOT NULL,
  `opis` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
