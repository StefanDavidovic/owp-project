-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: knjizara
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `knjige`
--

DROP TABLE IF EXISTS `knjige`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `knjige` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` varchar(256) NOT NULL,
  `isbn` varchar(13) DEFAULT NULL,
  `izdavackaKuca` varchar(75) DEFAULT NULL,
  `autor` varchar(75) DEFAULT NULL,
  `opis` varchar(256) DEFAULT NULL,
  `cena` decimal(8,2) DEFAULT NULL,
  `brStrana` int DEFAULT NULL,
  `povez` varchar(34) DEFAULT NULL,
  `pismo` varchar(34) DEFAULT NULL,
  `jezikPisanja` varchar(20) DEFAULT NULL,
  `prosecnaOcena` decimal(2,1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `isbn` (`isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `knjige`
--

LOCK TABLES `knjige` WRITE;
/*!40000 ALTER TABLE `knjige` DISABLE KEYS */;
INSERT INTO `knjige` VALUES (28,'Zelim te','1533213413224','Eduka','Ivo Andric','Nesooo bas',1100.00,233,'Tvrdi','latinica','Srpski',4.0),(29,'Tri Metra Iznad Neba','2212121121343','Zavod','Francesco','Romantika neka',2300.00,345,'Tvrdi','latinica','Srpski',4.0),(30,'Kostana','3534523432423','Zavod','Bora Stankovic','U Vranjee ',1400.00,234,'Tvrdi','latinica','Srpski',3.0),(31,'Zlocin i Kazna','2345214523531','Zavod','Dostojevski','Zlociniii i kaznice',2500.00,450,'Tvrdi','latinica','Srpski',4.0);
/*!40000 ALTER TABLE `knjige` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnici`
--

DROP TABLE IF EXISTS `korisnici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `korisnici` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `korisnickoIme` varchar(20) DEFAULT NULL,
  `lozinka` varchar(20) NOT NULL,
  `eMail` varchar(20) NOT NULL,
  `pol` varchar(20) DEFAULT NULL,
  `administrator` tinyint(1) DEFAULT '0',
  `datumRodjenja` date DEFAULT NULL,
  `adresa` varchar(75) DEFAULT NULL,
  `brTelefona` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnici`
--

LOCK TABLES `korisnici` WRITE;
/*!40000 ALTER TABLE `korisnici` DISABLE KEYS */;
INSERT INTO `korisnici` VALUES (10,'ivo','ivo','ivo@gmail.com','muški',1,'2012-02-02','Pupinova','2321232'),(11,'mare','mare123','mare@gmail.com','muški',1,'2021-01-01','Mikijeva','3243234');
/*!40000 ALTER TABLE `korisnici` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zanrovi`
--

DROP TABLE IF EXISTS `zanrovi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zanrovi` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `naziv` varchar(75) NOT NULL,
  `opis` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zanrovi`
--

LOCK TABLES `zanrovi` WRITE;
/*!40000 ALTER TABLE `zanrovi` DISABLE KEYS */;
INSERT INTO `zanrovi` VALUES (1,'naučna fantastika','bla bla'),(2,'akcija','run run'),(3,'komedija','smesno'),(4,'horor','strasno'),(5,'avantura','pustolovine');
/*!40000 ALTER TABLE `zanrovi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-13  9:36:01
