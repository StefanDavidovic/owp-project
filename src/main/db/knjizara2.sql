

INSERT INTO `knjizara`.`knjige`
(`id`,
`naziv`,
`isbn`,
`izdavackaKuca`,
`autor`,
`opis`,
`cena`,
`brStrana`,
`povez`,
`pismo`,
`jezikPisanja`,
`prosecnaOcena`)
VALUES
(<{id: }>,
<{naziv: }>,
<{isbn: }>,
<{izdavackaKuca: }>,
<{autor: }>,
<{opis: }>,
<{cena: }>,
<{brStrana: }>,
<{povez: }>,
<{pismo: }>,
<{jezikPisanja: }>,
<{prosecnaOcena: }>);

INSERT INTO `knjizara`.`korisnici`
(`id`,
`korisnickoIme`,
`lozinka`,
`eMail`,
`pol`,
`administrator`,
`datumRodjenja`,
`adresa`,
`brTelefona`)
VALUES
(<{id: }>,
<{korisnickoIme: }>,
<{lozinka: }>,
<{eMail: }>,
<{pol: }>,
<{administrator: 0}>,
<{datumRodjenja: }>,
<{adresa: }>,
<{brTelefona: }>);

INSERT INTO `knjizara`.`zanrovi`
(`id`,
`naziv`,
`opis`)
VALUES
(<{id: }>,
<{naziv: }>,
<{opis: }>);
SELECT * FROM knjizara.knjige;